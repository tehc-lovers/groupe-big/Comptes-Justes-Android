package be.helha.comptesjustes;

import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;

@SuppressLint( "CustomSplashScreen" )
public class SplashScreenActivity extends AppCompatActivity
{

    @Override
    protected void onCreate( Bundle savedInstanceState )
    {
        super.onCreate( savedInstanceState );
        setContentView( R.layout.activity_splash_screen );

        Intent intent = new Intent( SplashScreenActivity.this, MainActivity.class );
        startActivity( intent );
        finish();
    }
}
