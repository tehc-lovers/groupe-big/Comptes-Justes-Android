package be.helha.comptesjustes.infrastructure;

import java.util.List;

import be.helha.comptesjustes.dto.account.DtoAccount;
import be.helha.comptesjustes.dto.accountmanager.DtoCreateAccountManager;
import be.helha.comptesjustes.dto.accountuser.DtoCreateAccountUser;
import be.helha.comptesjustes.dto.user.DtoUser;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;

public interface IAccountManagerRepository
{
    @GET( "accountmanager/{id}" )
    Call< List< DtoAccount > > getAccountByManagerId( @Path ( "id" ) int idManager );

    @POST( "accountmanager" )
    Call< DtoCreateAccountManager > create( @Body DtoCreateAccountManager dtoCreateAccountManager );
}
