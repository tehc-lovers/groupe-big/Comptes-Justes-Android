package be.helha.comptesjustes.infrastructure;

import java.util.List;

import be.helha.comptesjustes.dto.transaction.DtoCreateTransaction;
import be.helha.comptesjustes.dto.transaction.DtoTransaction;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;

public interface ITransactionRepository
{
    @GET( "transactions" )
    Call< List< DtoTransaction > > getAll();

    @GET( "transactions/{id}" )
    Call< DtoTransaction > getById( @Path ( "id" ) int id );

    @POST( "transactions" )
    Call< DtoTransaction > create( @Body DtoCreateTransaction dtoCreateTransaction );
}
