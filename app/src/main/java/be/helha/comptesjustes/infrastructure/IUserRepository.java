package be.helha.comptesjustes.infrastructure;

import java.util.List;

import be.helha.comptesjustes.dto.user.DtoCreateUser;
import be.helha.comptesjustes.dto.user.DtoUser;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;

public interface IUserRepository
{
    @GET( "users" )
    Call< List< DtoUser > > getAll();

    @GET( "users/{id}" )
    Call< DtoUser > getById( @Path( "id" ) int id );

    @POST( "users" )
    Call< DtoUser > create( @Body DtoCreateUser dtoCreateUser );

    @PUT( "users/{id}" )
    Call< Void > update( @Body DtoUser dto, @Path( "id" ) int id );
}
