package be.helha.comptesjustes.infrastructure;

import java.util.List;

import be.helha.comptesjustes.dto.account.DtoAccount;
import be.helha.comptesjustes.dto.account.DtoCreateAccount;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;

public interface IAccountRepository
{
    @GET( "accounts" )
    Call< List< DtoAccount > > getAll();

    @GET( "accounts/{name}" )
    Call< DtoAccount > getByName( @Path( "name" ) String name );

    @POST( "accounts" )
    Call< DtoAccount > create( @Body DtoCreateAccount dtoAccount );
}
