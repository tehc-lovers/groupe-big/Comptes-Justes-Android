package be.helha.comptesjustes.infrastructure;

import java.util.List;

import be.helha.comptesjustes.dto.transaction.DtoCreateTransaction;
import be.helha.comptesjustes.dto.transaction.DtoTransaction;
import be.helha.comptesjustes.dto.transactionaccount.DtoCreateTransactionAccount;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;

public interface ITransactionAccountRepository
{
    @GET( "transactionAccount/{idAccount}" )
    Call< List< DtoTransaction > > getAll( @Path( "idAccount" ) int idAccount );

    @POST("transactionAccount")
    Call< DtoCreateTransactionAccount > create( @Body DtoCreateTransactionAccount dtoCreateTransactionAccount );
}
