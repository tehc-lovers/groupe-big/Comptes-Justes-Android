package be.helha.comptesjustes.infrastructure;

import java.util.List;

import be.helha.comptesjustes.dto.bill.DtoBill;
import be.helha.comptesjustes.dto.bill.DtoCreateBill;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;

public interface IBillRepository
{
    @GET( "bills" )
    Call< List< DtoBill > > getAll();

    @POST( "bills" )
    Call< DtoBill > create( @Body DtoCreateBill dtoCreateBill );
}
