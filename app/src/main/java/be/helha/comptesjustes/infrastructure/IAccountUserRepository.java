package be.helha.comptesjustes.infrastructure;

import java.util.List;

import be.helha.comptesjustes.dto.account.DtoAccount;
import be.helha.comptesjustes.dto.accountuser.DtoCreateAccountUser;
import be.helha.comptesjustes.dto.transaction.DtoTransaction;
import be.helha.comptesjustes.dto.user.DtoUser;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;

public interface IAccountUserRepository
{
    @GET( "accountuser/IdUser/{idUser}" )
    Call< List< DtoAccount > > getAccountsByUserId( @Path( "idUser" ) int idUser );

    @GET( "accountuser/IdAccount/{idAccount}" )
    Call< List< DtoUser > > getUsersByAccountId( @Path( "idAccount" ) int idAccount );

    @POST( "accountuser" )
    Call< DtoCreateAccountUser > create( @Body DtoCreateAccountUser dtoCreateAccountUser );

    @DELETE( "accountuser/Delete/{idUser}/{idAccount}")
    Call< Void > delete( @Path( "idUser" ) int idUser, @Path( "idAccount" ) int idAccount );
}
