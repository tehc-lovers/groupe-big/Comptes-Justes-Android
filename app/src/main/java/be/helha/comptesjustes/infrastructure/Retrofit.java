package be.helha.comptesjustes.infrastructure;

import retrofit2.converter.gson.GsonConverterFactory;

public class Retrofit
{
    /* Virtual Phone */
    private static final String BASE_URL = "http://10.0.2.2:5000/api/";

    /* Calata.be */
    /*private static final String BASE_URL = "http://www.calata.be:5000/api/";*/

    /* Machine in same network */
    /* private static final String BASE_URL = "http://10.2.33.69:5000/api/"; */


    private static retrofit2.Retrofit instance;

    public static retrofit2.Retrofit getInstance()
    {
        if ( instance == null )
        {
            instance = new retrofit2.Retrofit.Builder()
                    .baseUrl( BASE_URL )
                    .addConverterFactory( GsonConverterFactory.create() )
                    .build();
        }
        return instance;
    }
}
