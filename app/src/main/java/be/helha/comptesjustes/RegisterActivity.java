package be.helha.comptesjustes;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

import be.helha.comptesjustes.dto.user.DtoCreateUser;
import be.helha.comptesjustes.dto.user.DtoUser;
import be.helha.comptesjustes.infrastructure.IUserRepository;
import be.helha.comptesjustes.infrastructure.Retrofit;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RegisterActivity extends AppCompatActivity
{
    private EditText etUsername, etFirstName, etLastName, etEmailAddress, etAccountNumber, etPassword;
    private Spinner sAccountNumberPrefix;
    private Button btnSubmit;

    @Override
    protected void onCreate( Bundle savedInstanceState )
    {
        super.onCreate( savedInstanceState );
        setContentView( R.layout.activity_register );

        initViewInstances();
        initListeners();
    }

    private void initViewInstances()
    {
        etUsername = findViewById( R.id.et_registerActivity_username );
        etFirstName = findViewById( R.id.et_registerActivity_firstName );
        etLastName = findViewById( R.id.et_registerActivity_lastName );
        etEmailAddress = findViewById( R.id.et_registerActivity_emailAddress );
        etAccountNumber = findViewById( R.id.et_registerActivity_accountNumber );
        etPassword = findViewById( R.id.et_registerActivity_password );
        sAccountNumberPrefix = findViewById( R.id.s_registerActivity_accountNumberPrefix );
        btnSubmit = findViewById( R.id.btn_registerActivity_submit );
    }

    private void initListeners()
    {
        ArrayAdapter< CharSequence > spinnerAdapter = ArrayAdapter.createFromResource( this, R.array.account_prefixes, android.R.layout.simple_spinner_item );
        spinnerAdapter.setDropDownViewResource( android.R.layout.simple_spinner_dropdown_item );
        sAccountNumberPrefix.setAdapter( spinnerAdapter );

        btnSubmit.setOnClickListener( new View.OnClickListener()
        {
            @Override
            public void onClick( View v )
            {
                boolean emptyString = false;
                String strUsername = etUsername.getText().toString();
                String strFirstName = etFirstName.getText().toString();
                String strLastName = etLastName.getText().toString();
                String strEmailAddress = etEmailAddress.getText().toString();
                String strAccountNumber = sAccountNumberPrefix.getSelectedItem().toString() + etAccountNumber.getText().toString().toUpperCase();
                String strPassword = etPassword.getText().toString();
                String[] strings =
                        {
                                strUsername,
                                strFirstName,
                                strLastName,
                                strEmailAddress,
                                strAccountNumber,
                                strPassword
                        };
                for( String s : strings )
                {
                    if( s.isEmpty() )
                    {
                        emptyString = true;
                        break;
                    }
                }
                if( emptyString )
                {
                    Log.d( "Error", "Empty String Not Allowed" );
                    Toast.makeText( getApplicationContext(), "Empty Input Not Allowed", Toast.LENGTH_LONG ).show();
                }
                else if( strPassword.length() < 8 )
                {
                    Log.e( "Error", "Password less than 8 characters" );
                    Toast.makeText( getApplicationContext(), "Password less than 8 characters", Toast.LENGTH_SHORT ).show();
                }
                else if( !strEmailAddress.contains( "@" ) ||
                        !strEmailAddress.split( "@" )[1].contains( "." ) ||
                        strEmailAddress.split( "@" ).length > 2 )
                {
                    Log.e( "Error", "Not an email address" );
                    Toast.makeText( getApplicationContext(), "Not an email address", Toast.LENGTH_SHORT ).show();
                }
                else
                {
                    DtoCreateUser dtoCreateUser = new DtoCreateUser( strFirstName, strLastName, strEmailAddress, strUsername, strPassword, strAccountNumber );
                    Retrofit.getInstance().create( IUserRepository.class ).getAll().enqueue( new Callback< List< DtoUser > >()
                    {
                        @Override
                        public void onResponse( @NonNull Call< List< DtoUser > > call, @NonNull Response< List< DtoUser > > response )
                        {
                            boolean userExists = false;
                            assert response.body() != null;
                            Log.d( "Users", String.valueOf( response.body() ) );
                            for( DtoUser dtoUser : response.body() )
                            {
                                if ( dtoUser.getNickname().equalsIgnoreCase( dtoCreateUser.getNickname() ) ||
                                        dtoUser.getEmail().equalsIgnoreCase( dtoCreateUser.getEmail() ) )
                                {
                                    userExists = true;
                                    break;
                                }
                            }
                            if ( userExists )
                            {
                                Log.d( "User Creation Error", "User Already Exists" );
                                Toast.makeText( getApplicationContext(), "User Already Exists", Toast.LENGTH_LONG ).show();
                                finish();
                            }
                            else
                            {
                                final Handler handler = new Handler( Looper.getMainLooper() );
                                handler.postDelayed( new Runnable()
                                {
                                    @Override
                                    public void run()
                                    {

                                        Retrofit.getInstance().create( IUserRepository.class ).create( dtoCreateUser ).enqueue( new Callback< DtoUser >()
                                        {
                                            @Override
                                            public void onResponse( @NonNull Call< DtoUser > call, @NonNull Response< DtoUser > response )
                                            {
                                                Log.d( "Created User", String.valueOf( response.body() ) );
                                                Toast.makeText( getApplicationContext(), "Registration Successful", Toast.LENGTH_SHORT ).show();
                                            }

                                            @Override
                                            public void onFailure( @NonNull Call< DtoUser > call, @NonNull Throwable t )
                                            {
                                                Log.e( "Error", t.toString() );
                                            }
                                        } );
                                    }
                                }, 2000 );
                            }
                        }

                        @Override
                        public void onFailure( @NonNull Call< List< DtoUser > > call, @NonNull Throwable t )
                        {
                            Log.e( "Error", t.toString() );
                        }
                    } );
                }
                finish();
            }
        } );
    }
}
