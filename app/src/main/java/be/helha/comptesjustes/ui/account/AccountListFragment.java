package be.helha.comptesjustes.ui.account;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.google.android.material.dialog.MaterialAlertDialogBuilder;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.gson.Gson;
import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import be.helha.comptesjustes.MainActivity;
import be.helha.comptesjustes.R;
import be.helha.comptesjustes.dto.account.DtoAccount;
import be.helha.comptesjustes.dto.account.DtoCreateAccount;
import be.helha.comptesjustes.dto.accountuser.DtoCreateAccountUser;
import be.helha.comptesjustes.infrastructure.IAccountRepository;
import be.helha.comptesjustes.infrastructure.IAccountUserRepository;
import be.helha.comptesjustes.infrastructure.Retrofit;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A fragment representing a list of Items.
 */
public class AccountListFragment extends Fragment
{

    private static final String ARG_COLUMN_COUNT = "column-count";
    private static final String ARG_USER_ID = "user-id";
    private List< DtoAccount > accountList;
    private static int mColumnCount = 1;
    private AccountListRecyclerViewAdapter accountListRecyclerViewAdapter;
    private static SharedPreferences.Editor editor;
    private static Gson gson;
    private RecyclerView recyclerView;
    private int userId;

    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public AccountListFragment()
    {
    }

    // TODO: Customize parameter initialization
    @SuppressWarnings( "unused" )
    public static AccountListFragment newInstance( int id )
    {
        AccountListFragment fragment = new AccountListFragment();
        Bundle args = new Bundle();
        args.putInt( ARG_COLUMN_COUNT, mColumnCount );
        args.putInt( ARG_USER_ID, id );
        fragment.setArguments( args );
        return fragment;
    }

    @Override
    public void onCreate( Bundle savedInstanceState )
    {
        super.onCreate( savedInstanceState );

        if ( getArguments() != null )
        {
            mColumnCount = getArguments().getInt( ARG_COLUMN_COUNT );
            userId = getArguments().getInt( ARG_USER_ID );
        }
        try
        {
            accountList = new ArrayList<>();
            OnAccountClickListener clickListener = ( OnAccountClickListener ) getActivity();
            accountListRecyclerViewAdapter = new AccountListRecyclerViewAdapter( accountList, clickListener, getContext() );
            fetchAccountList();
            gson = new Gson();
            String strAccountsId = String.format( "accounts_%d", userId );
            SharedPreferences preferences = requireActivity().getSharedPreferences( strAccountsId, Context.MODE_PRIVATE );
            if( preferences.contains( strAccountsId ) )
            {
                preferences.edit().remove( strAccountsId ).apply();
            }
            String res = preferences.getString( strAccountsId, "[]" );
            editor = preferences.edit();
            DtoAccount[] accountsArray = gson.fromJson( res, DtoAccount[].class );
            for( DtoAccount dtoAccount : accountsArray )
            {
                if( accountList.contains( dtoAccount ) )
                {
                    continue;
                }
                accountList.add( dtoAccount );
            }
        }
        catch ( ClassCastException cce )
        {
            Log.e( "FragmentCastException", "The main activity must implement the interface OnAccountClickListener" );
        }
        catch ( AssertionError | Exception e )
        {
            Log.e( "Error", e.getMessage() );
        }
    }

    private void autoScroll()
    {
        if( accountListRecyclerViewAdapter.getItemCount() > 0 )
        {
            recyclerView.post( new Runnable()
            {
                @Override
                public void run()
                {
                    recyclerView.smoothScrollToPosition( accountListRecyclerViewAdapter.getItemCount() - 1 );
                }
            } );
        }
    }

    @Override
    public void onResume()
    {
        super.onResume();

        try
        {
            final Handler handler = new Handler( Looper.getMainLooper() );
            handler.postDelayed( new Runnable()
            {
                @Override
                public void run()
                {
                    fetchAccountList();
                }
            }, 1000 );
        }
        catch ( AssertionError | Exception e )
        {
            Log.e( "Error", e.getMessage() );
        }

    }

    private void fetchAccountList()
    {
        Retrofit.getInstance().create( IAccountUserRepository.class ).getAccountsByUserId( userId ).enqueue( new Callback< List< DtoAccount > >()
        {
            @Override
            public void onResponse( @NonNull Call< List< DtoAccount > > call, @NonNull Response< List< DtoAccount > > response )
            {
                try
                {
                    assert response.body() != null;
                    for( DtoAccount dtoAccount : response.body() )
                    {
                        if( accountList.contains( dtoAccount ) )
                        {
                            continue;
                        }
                        accountList.add( dtoAccount );
                    }
                    accountListRecyclerViewAdapter.notifyItemChanged( 0 );
                    autoScroll();
                    saveAccounts();
                }
                catch ( Error | Exception e )
                {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure( @NonNull Call< List< DtoAccount > > call, @NonNull Throwable t )
            {
                Log.e( "Error", t.toString() );
            }
        } );
    }

    @Override
    public void onCreateOptionsMenu( @NonNull Menu menu, @NonNull MenuInflater inflater )
    {
        inflater.inflate( R.menu.menu_account_list_fragment_top, menu );
        super.onCreateOptionsMenu( menu, inflater );
    }

    @Override
    public boolean onOptionsItemSelected( @NonNull MenuItem item )
    {
        if ( item.getItemId() == R.id.item_accountListFragment_info )
        {
            MaterialAlertDialogBuilder builder = new MaterialAlertDialogBuilder( requireContext() );
            builder.setMessage( R.string.dialog_accountListFragment_info )
                    .setPositiveButton( "Understood", null )
                    .show();
            return true;
        }
        if( item.getItemId() == R.id.item_accountListFragment_readQRCode )
        {
            IntentIntegrator intentIntegrator = new IntentIntegrator( getActivity() );
            intentIntegrator.setPrompt( "Scan a barcode or QR code" );
            intentIntegrator.setOrientationLocked( true );
            IntentIntegrator.forSupportFragment( AccountListFragment.this ).initiateScan();
            return true;
        }
        return false;
    }

    @Override
    public void onActivityResult( int requestCode, int resultCode, @Nullable Intent data )
    {
        super.onActivityResult( requestCode, resultCode, data );
        IntentResult intentResult = IntentIntegrator.parseActivityResult( requestCode, resultCode, data );
        if( intentResult != null )
        {
            if( intentResult.getContents() == null )
            {
                Log.e( "Error", "intentResult.getContents() = null" );
                Toast.makeText( getContext(), "Cancelled", Toast.LENGTH_SHORT ).show();
            }
            else
            {
                Gson gson = new Gson();
                DtoAccount dtoAccount = gson.fromJson( intentResult.getContents(), DtoAccount.class );
                DtoCreateAccountUser dtoCreateAccountUser = new DtoCreateAccountUser( dtoAccount.getId(), userId );
                MaterialAlertDialogBuilder builder = new MaterialAlertDialogBuilder( requireContext() );
                builder.setMessage( "Join account: " + dtoAccount.getName() + "?" )
                        .setPositiveButton( "Join", new DialogInterface.OnClickListener()
                        {
                            @Override
                            public void onClick( DialogInterface dialog, int which )
                            {

                                Retrofit.getInstance().create( IAccountUserRepository.class ).create( dtoCreateAccountUser ).enqueue( new Callback< DtoCreateAccountUser >()
                                {
                                    @Override
                                    public void onResponse( @NonNull Call< DtoCreateAccountUser > call, @NonNull Response< DtoCreateAccountUser > response )
                                    {
                                        Log.d( "Bond Created", "Account Joined From QR Code" );
                                        Toast.makeText( getContext(), "Account Joined Successfully, please refresh the page", Toast.LENGTH_SHORT ).show();
                                    }

                                    @Override
                                    public void onFailure( @NonNull Call< DtoCreateAccountUser > call, @NonNull Throwable t )
                                    {
                                        Log.e( "Error", t.toString() );
                                    }
                                } );
                            }
                        } )
                        .setNegativeButton( "Cancel", null )
                        .show();
            }
        }
        else
        {
            Log.e( "Error", "intentResult = null" );
            super.onActivityResult( requestCode, resultCode, data );
        }
    }

    @Override
    public View onCreateView( LayoutInflater inflater, ViewGroup container,
                              Bundle savedInstanceState )
    {
        View view = inflater.inflate( R.layout.fragment_account_list, container, false );

        AppCompatActivity activity = ( ( AppCompatActivity ) getActivity() );
        assert activity != null;
        ( Objects.requireNonNull( activity.getSupportActionBar() ) ).setTitle( getResources().getString( R.string.bottomNavBar_mainActivity_accounts ) );
        setHasOptionsMenu( true );

        FloatingActionButton fab = view.findViewById( R.id.fab_accountFragmentList_create );
        fab.setOnClickListener( new View.OnClickListener()
        {
            @Override
            public void onClick( View view )
            {
                Log.d( "Account FAB", "Create new account" );
                FragmentManager fragmentManager = getParentFragmentManager();
                FragmentTransaction transaction = fragmentManager.beginTransaction();
                transaction.addToBackStack( "account_create" );
                transaction.replace( R.id.fcv_activityMain_host, AccountCreateFragment.newInstance( userId, userId ) );
                transaction.commit();
            }
        } );
        Context context = view.getContext();
        recyclerView = view.findViewById( R.id.rv_fragmentAccountList_list );
        if ( mColumnCount <= 1 )
        {
            recyclerView.setLayoutManager( new LinearLayoutManager( context ) );
        } else
        {
            recyclerView.setLayoutManager( new GridLayoutManager( context, mColumnCount ) );
        }
        recyclerView.setAdapter( accountListRecyclerViewAdapter );

        return view;
    }

    public interface OnAccountClickListener
    {
        void onAccountClick( DtoAccount dtoAccount );
    }

    private void saveAccounts()
    {
        String accountJson = gson.toJson( accountList );
        editor.putString( String.format( "accounts_%d", userId ), accountJson );
        editor.apply();
        Log.i( "Account JSON", accountJson );
    }
}
