package be.helha.comptesjustes.ui.home;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import java.util.Objects;

import be.helha.comptesjustes.R;
import be.helha.comptesjustes.databinding.FragmentHomeBinding;

public class HomeFragment extends Fragment
{

    private HomeViewModel homeViewModel;
    private FragmentHomeBinding binding;

    public View onCreateView( @NonNull LayoutInflater inflater,
                              ViewGroup container, Bundle savedInstanceState )
    {
        homeViewModel =
                new ViewModelProvider( this ).get( HomeViewModel.class );

        binding = FragmentHomeBinding.inflate( inflater, container, false );
        View root = binding.getRoot();

        AppCompatActivity activity = ( ( AppCompatActivity ) getActivity() );
        assert activity != null;
        Objects.requireNonNull( activity.getSupportActionBar() ).setTitle( getResources().getString( R.string.app_name ) );
        setHasOptionsMenu( true );

        final TextView textView = binding.textHome;
        homeViewModel.getText().observe( getViewLifecycleOwner(), new Observer< String >()
        {
            @Override
            public void onChanged( @Nullable String s )
            {
                if( requireActivity().getSharedPreferences( "user", Context.MODE_PRIVATE ).contains( "userId" ) )
                {
                    textView.setText( requireActivity().getSharedPreferences( "user", Context.MODE_PRIVATE ).getString( "userNickname", "" ) );
                }
                else
                {
                    textView.setText( "You are currently disconnected, please log in or register" );
                }
            }
        } );
        return root;
    }

    @Override
    public void onResume()
    {
        super.onResume();
        final TextView textView = binding.textHome;
        homeViewModel.getText().observe( getViewLifecycleOwner(), new Observer< String >()
        {
            @Override
            public void onChanged( @Nullable String s )
            {
                SharedPreferences sharedPreferences = requireActivity().getSharedPreferences( "user", Context.MODE_PRIVATE );
                if( sharedPreferences.contains( "userId" ) )
                {
                    String welcomeMessage = getString( R.string.welcome ) + sharedPreferences.getString( "userNickname", "" );
                    textView.setText( welcomeMessage );
                }
                else
                {
                    textView.setText( "You are currently disconnected, please log in or register" );
                }
            }
        } );
    }

    @Override
    public void onCreateOptionsMenu( @NonNull Menu menu, @NonNull MenuInflater inflater )
    {
        super.onCreateOptionsMenu( menu, inflater );
    }

    @Override
    public void onDestroyView()
    {
        super.onDestroyView();
        binding = null;
    }
}
