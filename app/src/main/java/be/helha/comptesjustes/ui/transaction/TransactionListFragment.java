package be.helha.comptesjustes.ui.transaction;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.android.material.dialog.MaterialAlertDialogBuilder;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import be.helha.comptesjustes.R;
import be.helha.comptesjustes.dto.account.DtoAccount;
import be.helha.comptesjustes.dto.transaction.DtoTransaction;
import be.helha.comptesjustes.dto.user.DtoUser;
import be.helha.comptesjustes.infrastructure.IAccountManagerRepository;
import be.helha.comptesjustes.infrastructure.IAccountUserRepository;
import be.helha.comptesjustes.infrastructure.ITransactionAccountRepository;
import be.helha.comptesjustes.infrastructure.Retrofit;
import be.helha.comptesjustes.ui.account.AccountQRCodeWriterFragment;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A fragment representing a list of Items.
 */
public class TransactionListFragment extends Fragment
{

    private static final String ARG_COLUMN_COUNT = "column-count";
    private static final String ARG_ACCOUNT_NUMBER = "accountId";
    private List< DtoTransaction > transactionList;
    private static int mColumnCount = 1;
    private TransactionListRecyclerViewAdapter transactionListRecyclerViewAdapter;
    private static SharedPreferences.Editor editor;
    private static Gson gson;
    private int accountId;
    private int managerId;
    private TextView tvTotal;
    private RecyclerView recyclerView;
    private int nbParticipants = 0;

    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public TransactionListFragment()
    {
    }

    // TODO: Customize parameter initialization
    @SuppressWarnings( "unused" )
    public static TransactionListFragment newInstance( int id )
    {
        TransactionListFragment fragment = new TransactionListFragment();
        Bundle args = new Bundle();
        args.putInt( ARG_COLUMN_COUNT, mColumnCount );
        args.putInt( ARG_ACCOUNT_NUMBER, id );
        fragment.setArguments( args );
        return fragment;
    }

    @Override
    public void onCreate( Bundle savedInstanceState )
    {
        super.onCreate( savedInstanceState );
        Log.d( "Status", "Created" );

        if ( getArguments() != null )
        {
            mColumnCount = getArguments().getInt( ARG_COLUMN_COUNT );
            accountId = getArguments().getInt( ARG_ACCOUNT_NUMBER );
        }
        try
        {
            transactionList = new ArrayList<>();
            OnTransactionClickListener clickListener = ( OnTransactionClickListener ) getActivity();
            transactionListRecyclerViewAdapter = new TransactionListRecyclerViewAdapter( transactionList, clickListener );
            fetchTransactionList();
            gson = new Gson();
            SharedPreferences preferences = requireActivity().getSharedPreferences( "transactions", Context.MODE_PRIVATE );
            String res = preferences.getString( String.format( "transactions_%d", accountId ), "[]" );
            editor = preferences.edit();
            DtoTransaction[] transactionsArray = gson.fromJson( res, DtoTransaction[].class );
            for( DtoTransaction dtoTransaction : transactionsArray )
            {
                if( transactionList.contains( dtoTransaction ) )
                {
                    continue;
                }
                transactionList.add( dtoTransaction );
            }
        }
        catch ( ClassCastException cce )
        {
            Log.e( "FragmentCastException", "The activity must implement the interface OnTransactionClickListener" );
        }
        catch ( Error | Exception e )
        {
            Log.e( "Error", e.getMessage() );
        }
    }

    private void autoScroll()
    {
        if( transactionListRecyclerViewAdapter.getItemCount() > 0 )
        {
            recyclerView.post( new Runnable()
            {
                @Override
                public void run()
                {
                    recyclerView.smoothScrollToPosition( transactionListRecyclerViewAdapter.getItemCount() - 1 );
                }
            } );
        }
    }

    @Override
    public void onResume()
    {
        super.onResume();

        Log.d( "Status", "Resumed" );
        try
        {
            final Handler handler = new Handler( Looper.getMainLooper() );
            handler.postDelayed( new Runnable()
            {
                @Override
                public void run()
                {
                    fetchTransactionList();
                }
            }, 1000 );
        }
        catch ( Error | Exception e )
        {
            Log.e( "Error", e.getMessage() );
        }
    }

    private void fetchTransactionList()
    {
        Retrofit.getInstance().create( ITransactionAccountRepository.class ).getAll( accountId ).enqueue( new Callback< List< DtoTransaction > >()
        {
            @Override
            public void onResponse( @NonNull Call< List< DtoTransaction > > call, @NonNull Response< List< DtoTransaction > > response )
            {
                double total = 0;
                try
                {
                    assert response.body() != null;
                    for( DtoTransaction dtoTransaction : response.body() )
                    {
                        total += dtoTransaction.getAmount();
                        if( transactionList.contains( dtoTransaction ) )
                        {
                            continue;
                        }
                        transactionList.add( dtoTransaction );
                    }
                    transactionListRecyclerViewAdapter.notifyItemChanged( 0 );
                    Retrofit.getInstance().create( IAccountUserRepository.class ).getUsersByAccountId( accountId ).enqueue( new Callback< List< DtoUser > >()
                    {
                        @Override
                        public void onResponse( @NonNull Call< List< DtoUser > > call, @NonNull Response< List< DtoUser > > response )
                        {
                            assert response.body() != null;
                            nbParticipants = response.body().size();
                        }

                        @Override
                        public void onFailure( @NonNull Call< List< DtoUser > > call, @NonNull Throwable t )
                        {
                            Log.e( "Error", t.toString() );
                        }
                    } );
                    String strTotal = String.format( "%s: %.2f€", getString( R.string.tv_fragmentTransactionList_total ), total );
                    String strNbParticipants = String.format( "%s: %d", getString( R.string.tv_fragmentTransactionList_participants ), nbParticipants );
                    String strAmountPerPerson = String.format( "%s: %.2f€", getString( R.string.tv_fragmentTransactionList_amountPerPerson ), total / nbParticipants );
                    tvTotal.setText( String.format( "%s\n%s\n%s", strTotal, strNbParticipants, strAmountPerPerson ) );
                    autoScroll();
                    saveTransactions();
                }
                catch ( Error | Exception e )
                {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure( @NonNull Call< List< DtoTransaction > > call, @NonNull Throwable t )
            {
                Log.e( "Error", t.toString() );
            }
        } );
    }

    @Override
    public void onCreateOptionsMenu( @NonNull Menu menu, @NonNull MenuInflater inflater )
    {
        inflater.inflate( R.menu.menu_transaction_list_fragment_top, menu );
        super.onCreateOptionsMenu( menu, inflater );
    }

    @Override
    public boolean onOptionsItemSelected( @NonNull MenuItem item )
    {
        if( item.getItemId() == R.id.item_transactionListFragment_info )
        {
            MaterialAlertDialogBuilder builder = new MaterialAlertDialogBuilder( requireContext() );
            builder.setMessage( R.string.dialog_transactionListFragment_info )
                    .setPositiveButton( "Understood", null )
                    .show();
            return true;
        }
        if( item.getItemId() == R.id.item_transactionListFragment_qr )
        {
            FragmentManager fragmentManager = getParentFragmentManager();
            FragmentTransaction transaction = fragmentManager.beginTransaction();
            transaction.addToBackStack( "account_qr" );
            transaction.replace( R.id.fcv_activityMain_host, AccountQRCodeWriterFragment.newInstance( accountId ) );
            transaction.commit();
            return true;
        }
        return false;
    }

    @Override
    public View onCreateView( LayoutInflater inflater, ViewGroup container,
                              Bundle savedInstanceState )
    {
        View view = inflater.inflate( R.layout.fragment_transaction_list, container, false );

        AppCompatActivity activity = ( ( AppCompatActivity ) getActivity() );
        assert activity != null;
        Objects.requireNonNull( activity.getSupportActionBar() ).setTitle( getResources().getString( R.string.topNavBar_mainActivity_transactions ) );
        setHasOptionsMenu( true );

        tvTotal = view.findViewById( R.id.tv_fragmentTransactionList_total );


        FloatingActionButton fab = view.findViewById( R.id.fab_transactionFragmentList_create );
        fab.setVisibility( View.GONE );
        int userId = requireActivity().getSharedPreferences( "user", Context.MODE_PRIVATE ).getInt( "userId", 0 );
        Log.d( "Manager ID", String.valueOf( userId ) );
        Retrofit.getInstance().create( IAccountManagerRepository.class ).getAccountByManagerId( userId ).enqueue( new Callback< List< DtoAccount > >()
        {
            @Override
            public void onResponse( @NonNull Call< List< DtoAccount > > call, @NonNull Response< List< DtoAccount > > response )
            {
                assert response.body() != null;
                for( DtoAccount dtoAccount : response.body() )
                {
                    if( dtoAccount.getId() == accountId )
                    {
                        fab.setVisibility( View.VISIBLE );
                    }
                }
            }

            @Override
            public void onFailure( @NonNull Call< List< DtoAccount > > call, @NonNull Throwable t )
            {
                Log.e( "Error", t.toString() );
            }
        } );
        fab.setOnClickListener( new View.OnClickListener()
        {
            @Override
            public void onClick( View v )
            {
                Log.d( "FAB Transaction", "Create Transaction Opened" );
                FragmentManager fragmentManager = getParentFragmentManager();
                FragmentTransaction transaction = fragmentManager.beginTransaction();
                transaction.addToBackStack( "transaction_create" );
                transaction.replace( R.id.fcv_activityMain_host, TransactionCreateFragment.newInstance( accountId, userId ) );
                transaction.commit();
            }
        } );
        Context context = view.getContext();
        recyclerView = view.findViewById( R.id.rv_transactionListFragment_list );
        if ( mColumnCount <= 1 )
        {
            recyclerView.setLayoutManager( new LinearLayoutManager( context ) );
        } else
        {
            recyclerView.setLayoutManager( new GridLayoutManager( context, mColumnCount ) );
        }
        recyclerView.setAdapter( transactionListRecyclerViewAdapter );

        return view;
    }

    public interface OnTransactionClickListener
    {
        void onTransactionClick( DtoTransaction dtoTransaction );
    }

    private void saveTransactions()
    {
        String bookmarkJson = gson.toJson( transactionList );
        editor.putString( String.format( "transactions_%d", accountId ), bookmarkJson );
        editor.apply();
        Log.i( "Transaction JSON", bookmarkJson );
    }
}
