package be.helha.comptesjustes.ui.user;

import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.dialog.MaterialAlertDialogBuilder;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import be.helha.comptesjustes.databinding.FragmentUserItemBinding;
import be.helha.comptesjustes.dto.account.DtoAccount;
import be.helha.comptesjustes.dto.accountuser.DtoCreateAccountUser;
import be.helha.comptesjustes.dto.user.DtoUser;
import be.helha.comptesjustes.infrastructure.IAccountManagerRepository;
import be.helha.comptesjustes.infrastructure.IAccountRepository;
import be.helha.comptesjustes.infrastructure.IAccountUserRepository;
import be.helha.comptesjustes.infrastructure.Retrofit;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * {@link RecyclerView.Adapter} that can display a {@link DtoUser}.
 * TODO: Replace the implementation with code for your data type.
 */
public class UserRecyclerViewAdapter extends RecyclerView.Adapter< UserRecyclerViewAdapter.ViewHolder >
{
    private final List< DtoUser > mValues;
    private final UserListFragment.OnUserClickListener userClickListener;

    public UserRecyclerViewAdapter( List< DtoUser > items, UserListFragment.OnUserClickListener userClickListener )
    {
        mValues = items;
        this.userClickListener = userClickListener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder( @NonNull ViewGroup parent, int viewType )
    {

        return new ViewHolder( FragmentUserItemBinding.inflate( LayoutInflater.from( parent.getContext() ), parent, false ) );

    }

    @Override
    public void onBindViewHolder( final ViewHolder holder, int position )
    {
        final DtoUser dtoUser = mValues.get( position );
        holder.mItem = dtoUser;
        holder.tvId.setText( String.valueOf( dtoUser.getId() ) );
        holder.tvFirstName.setText( dtoUser.getFirstName() );
        holder.tvLastName.setText( dtoUser.getLastName() );
        holder.tvEmail.setText( dtoUser.getEmail() );
        holder.tvNickname.setText( dtoUser.getNickname() );
        holder.tvAccountNumber.setText( dtoUser.getAccountNumber() );
        holder.bind( userClickListener );
    }

    @Override
    public int getItemCount()
    {
        return mValues.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder
    {
        public final TextView tvId;
        public final TextView tvFirstName;
        public final TextView tvLastName;
        public final TextView tvEmail;
        public final TextView tvNickname;
        public final TextView tvAccountNumber;
        public DtoUser mItem;

        public ViewHolder( FragmentUserItemBinding binding )
        {
            super( binding.getRoot() );
            tvId = binding.tvFragmentUserListId;
            tvFirstName = binding.tvFragmentUserListFirstName;
            tvLastName = binding.tvFragmentUserListLastName;
            tvEmail = binding.tvFragmentUserListEmail;
            tvNickname = binding.tvFragmentUserListNickname;
            tvAccountNumber = binding.tvFragmentUserListAccountNumber;
        }

        public void bind( UserListFragment.OnUserClickListener onUserClickListener )
        {
            itemView.setOnClickListener( v -> onUserClickListener.onUserClick( mItem ) );
            itemView.setOnLongClickListener( v ->
            {
                Map< String, DtoAccount > dtoAccountMap = new HashMap<>();
                ArrayList< DtoAccount > accounts = new ArrayList<>();
                ArrayList< String > accountNames = new ArrayList<>();
                DialogInterface.OnMultiChoiceClickListener dialogClickListener = new DialogInterface.OnMultiChoiceClickListener()
                {
                    @Override
                    public void onClick( DialogInterface dialog, int which, boolean isChecked )
                    {
                        DtoAccount dtoAccount = dtoAccountMap.get( accountNames.get( which ) );
                        assert dtoAccount != null;
                        if( isChecked )
                        {
                            DtoCreateAccountUser dtoCreateAccountUser = new DtoCreateAccountUser( dtoAccount.getId(), mItem.getId() );
                            Retrofit.getInstance().create( IAccountUserRepository.class ).create( dtoCreateAccountUser ).enqueue( new Callback< DtoCreateAccountUser >()
                            {
                                @Override
                                public void onResponse( @NonNull Call< DtoCreateAccountUser > call, @NonNull Response< DtoCreateAccountUser > response )
                                {
                                    Log.d( "Account-User Created", String.valueOf( response.body() ) );
                                }

                                @Override
                                public void onFailure( @NonNull Call< DtoCreateAccountUser > call, @NonNull Throwable t )
                                {
                                    Log.e( "Error", t.toString() );
                                }
                            } );
                        }
                        else
                        {
                            Retrofit.getInstance().create( IAccountManagerRepository.class ).getAccountByManagerId( mItem.getId() ).enqueue( new Callback< List< DtoAccount > >()
                            {
                                @Override
                                public void onResponse( @NonNull Call< List< DtoAccount > > call, @NonNull Response< List< DtoAccount > > response )
                                {
                                    boolean isManagerAccount = false;
                                    boolean confirmRemoveManager = false;
                                    assert response.body() != null;
                                    for( DtoAccount account : response.body() )
                                    {
                                        if( account.getId() == dtoAccount.getId() )
                                        {
                                            isManagerAccount = true;
                                            break;
                                        }
                                    }
                                    if( isManagerAccount )
                                    {
                                        MaterialAlertDialogBuilder builder = new MaterialAlertDialogBuilder( v.getContext() );
                                        builder.setTitle( "Cannot remove admin this way, an admin must choose remove itself" )
                                                .setPositiveButton( "Understood", null )
                                                .show();
                                    }
                                    else
                                    {
                                        Retrofit.getInstance().create( IAccountUserRepository.class ).delete( mItem.getId(), dtoAccount.getId() ).enqueue( new Callback< Void >()
                                        {
                                            @Override
                                            public void onResponse( @NonNull Call< Void > call, @NonNull Response< Void > response )
                                            {
                                                Log.d( "Deletion", "Successful" );
                                            }

                                            @Override
                                            public void onFailure( @NonNull Call< Void > call, @NonNull Throwable t )
                                            {
                                                Log.e( "Error", t.toString() );
                                            }
                                        } );
                                    }
                                }

                                @Override
                                public void onFailure( @NonNull Call< List< DtoAccount > > call, @NonNull Throwable t )
                                {

                                }
                            } );
                        }
                    }
                };
                Retrofit.getInstance().create( IAccountRepository.class ).getAll().enqueue( new Callback< List< DtoAccount > >()
                {
                    @Override
                    public void onResponse( @NonNull Call< List< DtoAccount > > call, @NonNull Response< List< DtoAccount > > response )
                    {
                        assert response.body() != null;
                        accounts.addAll( response.body() );
                        for ( DtoAccount dtoAccount : accounts )
                        {
                            dtoAccountMap.put( dtoAccount.getName(), dtoAccount );
                        }
                        for( String s : dtoAccountMap.keySet() )
                        {
                            Log.d( "Key", s );
                            accountNames.add( s );
                        }
                        String[] accountNamesArray = accountNames.toArray( new String[ 0 ] );
                        Retrofit.getInstance().create( IAccountUserRepository.class ).getAccountsByUserId( mItem.getId() ).enqueue( new Callback< List< DtoAccount > >()
                        {
                            @Override
                            public void onResponse( @NonNull Call< List< DtoAccount > > call, @NonNull Response< List< DtoAccount > > response )
                            {
                                ArrayList< Boolean > accountsJoined = new ArrayList<>();
                                for( DtoAccount dtoAccount : dtoAccountMap.values() )
                                {
                                    assert response.body() != null;
                                    if ( response.body().contains( dtoAccount ) )
                                    {
                                        accountsJoined.add( true );
                                    }
                                    else
                                    {
                                        accountsJoined.add( false );
                                    }
                                }
                                boolean[] accountsJoinedArray = new boolean[ accountsJoined.size() ];
                                for( int i = 0; i < accountsJoined.size(); i++ )
                                {
                                    accountsJoinedArray[ i ] = accountsJoined.get( i );
                                }
                                MaterialAlertDialogBuilder builder = new MaterialAlertDialogBuilder( itemView.getContext() );
                                builder.setTitle( "Invite to or Remove from Account" )
                                        .setPositiveButton( "Apply", new DialogInterface.OnClickListener()
                                        {
                                            @Override
                                            public void onClick( DialogInterface dialog, int which )
                                            {
                                            }
                                        } )
                                        .setNegativeButton( "Cancel", null )
                                        .setMultiChoiceItems( accountNamesArray, accountsJoinedArray, dialogClickListener )
                                        .show();
                            }

                            @Override
                            public void onFailure( @NonNull Call< List< DtoAccount > > call, @NonNull Throwable t )
                            {
                                Log.e( "Error", t.toString() );
                            }
                        } );
                    }

                    @Override
                    public void onFailure( Call< List< DtoAccount > > call, Throwable t )
                    {

                    }
                } );
                return false;
            } );
        }

        @Override
        public String toString()
        {
            return "ViewHolder{" +
                    "tvId=" + tvId +
                    ", tvFirstName=" + tvFirstName +
                    ", tvLastName=" + tvLastName +
                    ", tvEmail=" + tvEmail +
                    ", tvNickname=" + tvNickname +
                    ", tvAccountNumber=" + tvAccountNumber +
                    '}';
        }
    }
}
