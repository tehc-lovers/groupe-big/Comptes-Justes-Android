package be.helha.comptesjustes.ui.transaction;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.gson.Gson;

import be.helha.comptesjustes.R;
import be.helha.comptesjustes.dto.transaction.DtoTransaction;
import be.helha.comptesjustes.infrastructure.ITransactionRepository;
import be.helha.comptesjustes.infrastructure.Retrofit;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link TransactionDetailFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class TransactionDetailFragment extends Fragment
{
    private static final String ARG_TRANSACTION_ID = "transactionId";

    private TextView tvTransactionId;
    private TextView tvTransactionAmount;
    private TextView tvTransactionDate;
    private TextView tvTransactionCommunication;
    private TextView tvTransactionLevel;

    private int transactionId;
    private DtoTransaction dtoTransaction;
    private static SharedPreferences preferences;
    private static SharedPreferences.Editor editor;
    private static Gson gson;

    public TransactionDetailFragment()
    {
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment TransactionDetailFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static TransactionDetailFragment newInstance( int transactionId )
    {
        TransactionDetailFragment fragment = new TransactionDetailFragment();
        Bundle args = new Bundle();
        args.putInt( ARG_TRANSACTION_ID, transactionId );
        fragment.setArguments( args );
        return fragment;
    }

    @Override
    public void onCreate( Bundle savedInstanceState )
    {
        super.onCreate( savedInstanceState );
        if ( getArguments() != null )
        {
            transactionId = getArguments().getInt( ARG_TRANSACTION_ID );
            fetchTransaction();
        }
    }

    private void fetchTransaction()
    {
        Retrofit.getInstance().create( ITransactionRepository.class ).getById( transactionId ).enqueue( new Callback< DtoTransaction >()
        {
            @Override
            public void onResponse( @NonNull Call< DtoTransaction > call, @NonNull Response< DtoTransaction > response )
            {
                Log.i( "Transaction Retrieved:", String.valueOf( response.body() ) );
                dtoTransaction = response.body();
                Log.i( "Transaction Retrieved:", String.valueOf( dtoTransaction ) );
                updateView();

            }

            @Override
            public void onFailure( @NonNull Call< DtoTransaction > call, @NonNull Throwable t )
            {
                Log.e( "Error", t.toString() );
                gson = new Gson();
                preferences = requireActivity().getSharedPreferences( "transactions", Context.MODE_PRIVATE );
                String res = preferences.getString( "transactions", "[]" );
                editor = preferences.edit();
                DtoTransaction[] transactionsArray = gson.fromJson( res, DtoTransaction[].class );
                for( DtoTransaction transaction : transactionsArray )
                {
                    if( transaction.getId() == transactionId )
                    {
                        dtoTransaction = transaction;
                    }
                }
                updateView();
            }
        } );
    }

    private void updateView()
    {
        tvTransactionId.setText(
                String.format(
                        "Id: %s",
                        dtoTransaction.getId() )
        );
        tvTransactionAmount.setText(
                String.format(
                        "%s\n%.2f",
                        getString( R.string.tv_transaction_amount ),
                        dtoTransaction.getAmount()
                )
        );
        tvTransactionDate.setText(
                String.format(
                        "%s\n%s",
                        getString( R.string.tv_transaction_date ),
                        dtoTransaction.getDate()
                )
        );
        tvTransactionCommunication.setText(
                String.format(
                        "%s\n%s",
                        getString( R.string.tv_transaction_communication ),
                        dtoTransaction.getCommunication()
                )
        );
        tvTransactionLevel.setText(
                String.format(
                        "%s\n%s",
                        getString( R.string.tv_transaction_necessityLevel ),
                        dtoTransaction.getLevel()
                )
        );
    }

    @Override
    public View onCreateView( LayoutInflater inflater, ViewGroup container,
                              Bundle savedInstanceState )
    {
        // Inflate the layout for this fragment
        View view = inflater.inflate( R.layout.fragment_transaction_detail, container, false );
        tvTransactionId = view.findViewById( R.id.tv_fragmentTransactionDetail_id );
        tvTransactionAmount = view.findViewById( R.id.tv_fragmentTransactionDetail_amount );
        tvTransactionDate = view.findViewById( R.id.tv_fragmentTransactionDetail_date );
        tvTransactionCommunication = view.findViewById( R.id.tv_fragmentTransactionDetail_communication );
        tvTransactionLevel = view.findViewById( R.id.tv_fragmentTransactionDetail_necessityLevel );
        return view;
    }
}
