package be.helha.comptesjustes.ui.user;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import com.google.gson.Gson;

import java.util.Objects;

import be.helha.comptesjustes.R;
import be.helha.comptesjustes.dto.user.DtoUser;
import be.helha.comptesjustes.infrastructure.IUserRepository;
import be.helha.comptesjustes.infrastructure.Retrofit;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * User Detail Fragment
 * Meant to show a specific user's details.
 */
public class UserDetailFragment extends Fragment
{
    private static final String ARG_USER_ID = "userId";

    private TextView tvUserId;
    private TextView tvUserFirstName;
    private TextView tvUserLastName;
    private TextView tvUserNickname;
    private TextView tvUserEmail;
    private TextView tvUserAccountNumber;

    private int userId;
    private DtoUser dtoUser;
    private static SharedPreferences preferences;
    private static SharedPreferences.Editor editor;
    private static Gson gson;

    /**
     * This constructor is required by default.
     */
    public UserDetailFragment()
    {
    }

    /**
     * New instance of the fragment based on the user's ID.
     *
     * @param userId The ID of the user that has been selected.
     * @return A new instance of fragment UserDetailFragment.
     */
    public static UserDetailFragment newInstance( int userId )
    {
        UserDetailFragment fragment = new UserDetailFragment();
        Bundle args = new Bundle();
        args.putInt( ARG_USER_ID, userId );
        fragment.setArguments( args );
        return fragment;
    }

    @Override
    public void onCreate( Bundle savedInstanceState )
    {
        super.onCreate( savedInstanceState );
        if ( getArguments() != null )
        {
            userId = getArguments().getInt( ARG_USER_ID );
            fetchUser();
        }
    }

    /**
     * Function meant to execute a SQL query meant to retrieve a single user based on his/her ID.
     */
    private void fetchUser()
    {
        Retrofit.getInstance().create( IUserRepository.class ).getById( userId )
                .enqueue( new Callback< DtoUser >()
        {
            @Override
            public void onResponse( @NonNull Call< DtoUser > call, @NonNull Response< DtoUser > response )
            {
                Log.i( "User Retrieved: ", String.valueOf( response.body() ) );
                dtoUser = response.body();
                Log.i( "User Retrieved: ", String.valueOf( dtoUser ) );
                updateView();
            }

            @Override
            public void onFailure( @NonNull Call< DtoUser > call, @NonNull Throwable t )
            {
                Log.e( "Error: ", t.toString() );
                gson = new Gson();
                preferences = requireActivity().getSharedPreferences( "users", Context.MODE_PRIVATE );
                String res = preferences.getString( "users", "[]" );
                editor = preferences.edit();
                DtoUser[] usersArray = gson.fromJson( res, DtoUser[].class );
                for( DtoUser user : usersArray )
                {
                    if( user.getId() == userId )
                    {
                        dtoUser = user;
                    }
                }
                updateView();
            }
        } );
    }

    /**
     * Function meant to update the various placeholders with the user's information.
     */
    private void updateView()
    {
        tvUserId.setText( String.format( "ID: %s", dtoUser.getId() ) );
        tvUserFirstName.setText(
                String.format(
                        "%s\n%s",
                        getString( R.string.tv_userListFragment_firstName ),
                        dtoUser.getFirstName()
                )
        );
        tvUserLastName.setText(
                String.format(
                        "%s\n%s",
                        getString( R.string.tv_userListFragment_lastName ),
                        dtoUser.getLastName()
                )
        );
        tvUserNickname.setText(
                String.format(
                        "%s\n%s",
                        getString( R.string.tv_userListFragment_nickname ),
                        dtoUser.getNickname()
                )
        );
        tvUserEmail.setText(
                String.format(
                        "%s: %s",
                        getString( R.string.tv_userListFragment_email ),
                        dtoUser.getEmail()
                )
        );
        tvUserAccountNumber.setText(
                String.format(
                        "%s\n%s",
                        getString( R.string.tv_userListFragment_accountNumber ),
                        dtoUser.getAccountNumber()
                )
        );
    }

    @Override
    public View onCreateView( LayoutInflater inflater, ViewGroup container,
                              Bundle savedInstanceState )
    {
        View view = inflater.inflate( R.layout.fragment_user_detail, container, false );
        tvUserId = view.findViewById( R.id.tv_fragmentUserDetail_id );
        tvUserFirstName = view.findViewById( R.id.tv_fragmentUserDetail_firstName );
        tvUserLastName = view.findViewById( R.id.tv_fragmentUserDetail_lastName );
        tvUserNickname = view.findViewById( R.id.tv_fragmentUserDetail_nickname );
        tvUserEmail = view.findViewById( R.id.tv_fragmentUserDetail_email );
        tvUserAccountNumber = view.findViewById( R.id.tv_fragmentUserDetail_accountNumber );
        return view;
    }
}
