package be.helha.comptesjustes.ui.account;

import android.graphics.Bitmap;
import android.graphics.Color;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.google.android.material.dialog.MaterialAlertDialogBuilder;
import com.google.gson.Gson;
import com.google.zxing.BarcodeFormat;
import com.google.zxing.WriterException;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.qrcode.QRCodeWriter;

import java.util.List;
import java.util.Objects;

import be.helha.comptesjustes.R;
import be.helha.comptesjustes.dto.account.DtoAccount;
import be.helha.comptesjustes.infrastructure.IAccountRepository;
import be.helha.comptesjustes.infrastructure.Retrofit;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link AccountQRCodeWriterFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class AccountQRCodeWriterFragment extends Fragment
{

    private static final String ARG_ACCOUNT_ID = "account-id";

    private int accountId;

    public AccountQRCodeWriterFragment()
    {
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param accountId Parameter 1.
     * @return A new instance of fragment AccountQRCodeWriterFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static AccountQRCodeWriterFragment newInstance( int accountId )
    {
        AccountQRCodeWriterFragment fragment = new AccountQRCodeWriterFragment();
        Bundle args = new Bundle();
        args.putInt( ARG_ACCOUNT_ID, accountId );
        fragment.setArguments( args );
        return fragment;
    }

    @Override
    public void onCreate( Bundle savedInstanceState )
    {
        super.onCreate( savedInstanceState );
        if ( getArguments() != null )
        {
            accountId = getArguments().getInt( ARG_ACCOUNT_ID );
        }
    }

    @Override
    public void onCreateOptionsMenu( @NonNull Menu menu, @NonNull MenuInflater inflater )
    {
        inflater.inflate( R.menu.menu_account_qr_code_fragment_top, menu );
        super.onCreateOptionsMenu( menu, inflater );
    }

    @Override
    public boolean onOptionsItemSelected( @NonNull MenuItem item )
    {
        if( item.getItemId() == R.id.item_accountQRCodeWriterFragment_info )
        {
            MaterialAlertDialogBuilder builder = new MaterialAlertDialogBuilder( requireContext() );
            builder.setMessage( R.string.dialog_accountQRCodeFragment_info )
                    .setPositiveButton( "Understood", null )
                    .show();
            return true;
        }
        return false;
    }

    @Override
    public View onCreateView( LayoutInflater inflater, ViewGroup container,
                              Bundle savedInstanceState )
    {
        View view = inflater.inflate( R.layout.fragment_account_q_r_code, container, false );

        AppCompatActivity activity = ( ( AppCompatActivity ) getActivity() );
        assert activity != null;
        Objects.requireNonNull( activity.getSupportActionBar() ).setTitle( getResources().getString( R.string.appBar_accountQRCodeFragment_title ) );

        setHasOptionsMenu( true );

        ImageView ivQRCode = view.findViewById( R.id.im_fragmentAccountQRCode_qrCode );
        QRCodeWriter writer = new QRCodeWriter();

        Retrofit.getInstance().create( IAccountRepository.class ).getAll().enqueue( new Callback< List< DtoAccount > >()
        {
            @Override
            public void onResponse( @NonNull Call< List< DtoAccount > > call, @NonNull Response< List< DtoAccount > > response )
            {
                assert response.body() != null;
                for( DtoAccount dtoAccount : response.body() )
                {
                    if( dtoAccount.getId() == accountId )
                    {
                        try
                        {
                            Gson gson = new Gson();
                            String dtoAccountJSON = gson.toJson( dtoAccount );
                            BitMatrix bitMatrix = writer.encode( String.valueOf( dtoAccountJSON ), BarcodeFormat.QR_CODE, 512, 512 );
                            int width = bitMatrix.getWidth();
                            int height = bitMatrix.getHeight();
                            Bitmap bmp = Bitmap.createBitmap( width, height, Bitmap.Config.RGB_565 );
                            for( int x = 0; x < width; x++ )
                            {
                                for( int y = 0; y < height; y++ )
                                {
                                    bmp.setPixel( x, y, bitMatrix.get( x, y ) ? Color.BLACK : Color.WHITE );
                                }
                            }
                            ivQRCode.setImageBitmap( bmp );
                        } catch ( WriterException e )
                        {
                            e.printStackTrace();
                        }
                    }
                }
            }

            @Override
            public void onFailure( @NonNull Call< List< DtoAccount > > call, @NonNull Throwable t )
            {
                Log.e( "Error", t.toString() );
            }
        } );

        return view;
    }
}
