package be.helha.comptesjustes.ui.bill;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import java.util.Objects;

import be.helha.comptesjustes.R;
import be.helha.comptesjustes.databinding.FragmentBillsBinding;

public class BillsFragment extends Fragment
{

    private BillsViewModel billsViewModel;
    private FragmentBillsBinding binding;

    public View onCreateView( @NonNull LayoutInflater inflater,
                              ViewGroup container, Bundle savedInstanceState )
    {
        billsViewModel =
                new ViewModelProvider( this ).get( BillsViewModel.class );

        binding = FragmentBillsBinding.inflate( inflater, container, false );
        View root = binding.getRoot();

        AppCompatActivity activity = ( ( AppCompatActivity ) getActivity() );
        assert activity != null;
        Objects.requireNonNull( activity.getSupportActionBar() ).setTitle( getResources().getString( R.string.bottomNavBar_mainActivity_bills ) );
        setHasOptionsMenu( true );

        final TextView textView = binding.textBills;
        billsViewModel.getText().observe( getViewLifecycleOwner(), new Observer< String >()
        {
            @Override
            public void onChanged( @Nullable String s )
            {
                textView.setText( s );
            }
        } );
        return root;
    }

    @Override
    public void onCreateOptionsMenu( @NonNull Menu menu, @NonNull MenuInflater inflater )
    {
        super.onCreateOptionsMenu( menu, inflater );
    }

    @Override
    public void onDestroyView()
    {
        super.onDestroyView();
        binding = null;
    }
}
