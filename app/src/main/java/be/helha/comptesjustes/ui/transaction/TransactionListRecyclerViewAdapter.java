package be.helha.comptesjustes.ui.transaction;

import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import be.helha.comptesjustes.databinding.FragmentTransactionItemBinding;
import be.helha.comptesjustes.dto.transaction.DtoTransaction;

import java.util.List;

/**
 * TODO: Replace the implementation with code for your data type.
 */
public class TransactionListRecyclerViewAdapter extends RecyclerView.Adapter< TransactionListRecyclerViewAdapter.ViewHolder >
{

    private final List< DtoTransaction > mValues;
    private final TransactionListFragment.OnTransactionClickListener transactionClickListener;

    public TransactionListRecyclerViewAdapter( List< DtoTransaction > items, TransactionListFragment.OnTransactionClickListener transactionClickListener )
    {
        mValues = items;
        this.transactionClickListener = transactionClickListener;
    }

    @Override
    public ViewHolder onCreateViewHolder( ViewGroup parent, int viewType )
    {

        return new ViewHolder( FragmentTransactionItemBinding.inflate( LayoutInflater.from( parent.getContext() ), parent, false ) );

    }

    @Override
    public void onBindViewHolder( final ViewHolder holder, int position )
    {
        final DtoTransaction dtoTransaction = mValues.get( position );

        holder.mItem = dtoTransaction;
        holder.tvId.setText( String.valueOf( dtoTransaction.getId() ) );
        holder.tvAmount.setText( String.format( "%.2f", dtoTransaction.getAmount() ) );
        holder.tvDate.setText( dtoTransaction.getDate().toString() );
        holder.tvCommunication.setText( dtoTransaction.getCommunication() );
        holder.tvLevel.setText( String.valueOf( dtoTransaction.getLevel() ) );
        holder.bind( transactionClickListener );
    }

    @Override
    public int getItemCount()
    {
        return mValues.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder
    {
        public final TextView tvId;
        public final TextView tvAmount;
        public final TextView tvDate;
        public final TextView tvLevel;
        public final TextView tvCommunication;
        public DtoTransaction mItem;

        public ViewHolder( FragmentTransactionItemBinding binding )
        {
            super( binding.getRoot() );
            tvId = binding.tvFragmentTransactionListId;
            tvAmount = binding.tvFragmentTransactionListAmount;
            tvDate = binding.tvFragmentTransactionListDate;
            tvLevel = binding.tvFragmentTransactionListNecessityLevel;
            tvCommunication = binding.tvFragmentTransactionListCommunication;
        }

        public void bind( TransactionListFragment.OnTransactionClickListener onTransactionClickListener )
        {
            itemView.setOnClickListener( v -> onTransactionClickListener.onTransactionClick( mItem ) );
            itemView.setOnLongClickListener( v ->
            {
                Toast.makeText( v.getContext(), mItem.toString(), Toast.LENGTH_LONG  ).show();
                return false;
            });
        }

        @Override
        public String toString()
        {
            return super.toString() + " '" + tvAmount.getText() + "'";
        }
    }
}
