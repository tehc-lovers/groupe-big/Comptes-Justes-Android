package be.helha.comptesjustes.ui.user;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.dialog.MaterialAlertDialogBuilder;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import be.helha.comptesjustes.R;
import be.helha.comptesjustes.dto.user.DtoUser;
import be.helha.comptesjustes.infrastructure.IUserRepository;
import be.helha.comptesjustes.infrastructure.Retrofit;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A fragment representing a list of Items.
 */
public class UserListFragment extends Fragment
{

    private static final String ARG_COLUMN_COUNT = "column-count";
    private List< DtoUser > userList;
    private static int mColumnCount = 1;
    private UserRecyclerViewAdapter userRecyclerViewAdapter;
    private static SharedPreferences.Editor editor;
    private static Gson gson;
    private RecyclerView recyclerView;

    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public UserListFragment()
    {
    }

    public static UserListFragment newInstance()
    {
        UserListFragment fragment = new UserListFragment();
        Bundle args = new Bundle();
        args.putInt( ARG_COLUMN_COUNT, mColumnCount );
        fragment.setArguments( args );
        return fragment;
    }

    @Override
    public void onCreate( Bundle savedInstanceState )
    {
        super.onCreate( savedInstanceState );

        if ( getArguments() != null )
        {
            mColumnCount = getArguments().getInt( ARG_COLUMN_COUNT );
        }
        try
        {
            userList = new ArrayList<>();
            OnUserClickListener clickListener = ( OnUserClickListener ) getActivity();
            userRecyclerViewAdapter = new UserRecyclerViewAdapter( userList, clickListener );
            fetchUserList();
            gson = new Gson();
            SharedPreferences preferences = requireActivity().getSharedPreferences( "users", Context.MODE_PRIVATE );
            String res = preferences.getString( "users", "[]" );
            editor = preferences.edit();
            DtoUser[] usersArray = gson.fromJson( res, DtoUser[].class );
            for( DtoUser dtoUser : usersArray )
            {
                if ( userList.contains( dtoUser ) )
                {
                    continue;
                }
                userList.add( dtoUser );
            }
        } catch ( ClassCastException cce )
        {
            Log.e( "FragmentCastException", "The activity must implement the interface OnUserClickListener" );
        }
        catch ( Exception e )
        {
            Log.e( "Error", e.getMessage() );
        }
    }

    private void autoScroll()
    {
        if( userRecyclerViewAdapter.getItemCount() > 0 )
        {
            recyclerView.post( new Runnable()
            {
                @Override
                public void run()
                {
                    recyclerView.smoothScrollToPosition( userRecyclerViewAdapter.getItemCount() - 1 );
                }
            } );
        }
    }

    @Override
    public void onResume()
    {
        super.onResume();

        try
        {
            final Handler handler = new Handler( Looper.getMainLooper() );
            handler.postDelayed( new Runnable()
            {
                @Override
                public void run()
                {
                    fetchUserList();
                }
            }, 1000 );
        }
        catch ( Error | Exception e )
        {
            Log.e( "Error", e.getMessage() );
        }
    }

    private void fetchUserList()
    {
        Retrofit.getInstance().create( IUserRepository.class ).getAll().enqueue( new Callback< List< DtoUser > >()
        {
            @Override
            public void onResponse( @NonNull Call< List< DtoUser > > call, @NonNull Response< List< DtoUser > > response )
            {
                try
                {
                    assert response.body() != null;
                    for( DtoUser dtoUser : response.body() )
                    {
                        if ( userList.contains( dtoUser ) )
                        {
                            userList.set( dtoUser.getId() - 1, dtoUser );
                            continue;
                        }
                        userList.add( dtoUser );
                    }
                    userRecyclerViewAdapter.notifyItemChanged( 0 );
                    autoScroll();
                    saveUsers();
                }
                catch ( Error | Exception e )
                {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure( @NonNull Call< List< DtoUser > > call, @NonNull Throwable t )
            {
                Log.e( "Error: ", t.toString() );
            }
        } );


    }

    @Override
    public void onCreateOptionsMenu( @NonNull Menu menu, @NonNull MenuInflater inflater )
    {
        inflater.inflate( R.menu.menu_user_list_fragment_top, menu );
        super.onCreateOptionsMenu( menu, inflater );
    }

    @Override
    public boolean onOptionsItemSelected( @NonNull MenuItem item )
    {
        if( item.getItemId() == R.id.item_userListFragment_info )
        {
            MaterialAlertDialogBuilder builder = new MaterialAlertDialogBuilder( requireContext() );
            builder.setMessage( R.string.dialog_userListFragment_info )
                    .setPositiveButton( "Understood", null )
                    .show();
            return true;
        }
        return false;
    }

    @Override
    public View onCreateView( LayoutInflater inflater, ViewGroup container,
                              Bundle savedInstanceState )
    {
        View view = inflater.inflate( R.layout.fragment_user_list, container, false );

        AppCompatActivity activity = ( ( AppCompatActivity ) getActivity() );
        assert activity != null;
        Objects.requireNonNull( activity.getSupportActionBar() ).setTitle(
                getResources().getString( R.string.bottomNavBar_mainActivity_users ) );

        setHasOptionsMenu( true );

        Context context = view.getContext();
        recyclerView = ( RecyclerView ) view;
        if ( mColumnCount <= 1 )
        {
            recyclerView.setLayoutManager( new LinearLayoutManager( context ) );
        } else
        {
            recyclerView.setLayoutManager( new GridLayoutManager( context, mColumnCount ) );
        }
        recyclerView.setAdapter( userRecyclerViewAdapter );

        return view;
    }

    public interface OnUserClickListener
    {
        void onUserClick( DtoUser dtoUser );
    }

    private void saveUsers()
    {
        String bookmarkJson = gson.toJson( userList );
        editor.putString( "users", bookmarkJson );
        editor.apply();
        Log.i( "User JSON", bookmarkJson );
    }
}
