package be.helha.comptesjustes.ui.account;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.util.Objects;

import be.helha.comptesjustes.R;
import be.helha.comptesjustes.dto.account.DtoAccount;
import be.helha.comptesjustes.dto.account.DtoCreateAccount;
import be.helha.comptesjustes.dto.accountmanager.DtoCreateAccountManager;
import be.helha.comptesjustes.dto.accountuser.DtoCreateAccountUser;
import be.helha.comptesjustes.infrastructure.IAccountManagerRepository;
import be.helha.comptesjustes.infrastructure.IAccountRepository;
import be.helha.comptesjustes.infrastructure.IAccountUserRepository;
import be.helha.comptesjustes.infrastructure.Retrofit;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link AccountCreateFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class AccountCreateFragment extends Fragment
{

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_USER_ID = "admin-id";
    private static final String ARG_MANAGER_ID = "manager-id";

    // TODO: Rename and change types of parameters
    private int userId;
    private int managerId;

    public AccountCreateFragment()
    {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment AccountCreateFragment.
     */
    public static AccountCreateFragment newInstance( int userId, int managerId )
    {
        AccountCreateFragment fragment = new AccountCreateFragment();
        Bundle args = new Bundle();
        args.putInt( ARG_USER_ID, userId );
        args.putInt( ARG_MANAGER_ID, managerId );
        fragment.setArguments( args );
        return fragment;
    }

    @Override
    public void onCreate( Bundle savedInstanceState )
    {
        super.onCreate( savedInstanceState );
        if ( getArguments() != null )
        {
            userId = getArguments().getInt( ARG_USER_ID );
            managerId = getArguments().getInt( ARG_MANAGER_ID );
        }
    }

    @Override
    public View onCreateView( LayoutInflater inflater, ViewGroup container,
                              Bundle savedInstanceState )
    {
        View view = inflater.inflate( R.layout.fragment_account_create, container, false );

        EditText etName = view.findViewById( R.id.et_fragmentAccountCreate_name );
        Button btnSubmit = view.findViewById( R.id.btn_fragmentAccountCreate_submit );
        btnSubmit.setOnClickListener( new View.OnClickListener()
        {
            @Override
            public void onClick( View view )
            {
                Log.d( "Creating", "New Account" );
                String accountName = etName.getText().toString();
                if( accountName.isEmpty() )
                {
                    Log.e( "Error", "Empty input not allowed" );
                    Toast.makeText( getContext(), "Empty input not allowed", Toast.LENGTH_LONG ).show();
                }
                else
                {
                    DtoCreateAccount dtoCreateAccount = new DtoCreateAccount( etName.getText().toString() );
                    Retrofit.getInstance().create( IAccountRepository.class ).create( new DtoCreateAccount( etName.getText().toString() ) ).enqueue( new Callback< DtoAccount >()
                    {
                        @Override
                        public void onResponse( @NonNull Call< DtoAccount > call, @NonNull Response< DtoAccount > response )
                        {
                            assert response.body() != null;
                            Log.d( "Account Created", String.valueOf( response.body() ) );
                            DtoCreateAccountUser dtoCreateAccountUser = new DtoCreateAccountUser( response.body().getId(), userId );
                            Retrofit.getInstance().create( IAccountUserRepository.class ).create( dtoCreateAccountUser ).enqueue( new Callback< DtoCreateAccountUser >()
                            {
                                @Override
                                public void onResponse( @NonNull Call< DtoCreateAccountUser > call, @NonNull Response< DtoCreateAccountUser > response )
                                {
                                    Log.d( "User Bond Created", String.valueOf( response.body() ) );
                                }

                                @Override
                                public void onFailure( @NonNull Call< DtoCreateAccountUser > call, @NonNull Throwable t )
                                {
                                    Log.e( "Error", t.toString() );
                                }
                            } );
                            DtoCreateAccountManager dtoCreateAccountManager = new DtoCreateAccountManager( response.body().getId(), userId );
                            Log.d( "AccountManager", String.valueOf( dtoCreateAccountManager ) );
                            Retrofit.getInstance().create( IAccountManagerRepository.class ).create( dtoCreateAccountManager ).enqueue( new Callback< DtoCreateAccountManager >()
                            {
                                @Override
                                public void onResponse( @NonNull Call< DtoCreateAccountManager > call, @NonNull Response< DtoCreateAccountManager > response )
                                {
                                    Log.d( "Manager Bond Created", String.valueOf( response.body() ) );
                                }

                                @Override
                                public void onFailure( @NonNull Call< DtoCreateAccountManager > call, @NonNull Throwable t )
                                {
                                    Log.e( "Error", t.toString() );
                                }
                            } );
                        }

                        @Override
                        public void onFailure( @NonNull Call< DtoAccount > call, @NonNull Throwable t )
                        {
                            Log.e( "Error", t.toString() );
                        }
                    } );
                }
                FragmentManager fragmentManager = getParentFragmentManager();
                FragmentTransaction transaction = fragmentManager.beginTransaction();
                transaction.commit();
                fragmentManager.popBackStack();
            }
        } );

        return view;
    }
}
