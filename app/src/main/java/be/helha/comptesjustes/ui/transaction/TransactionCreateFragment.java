package be.helha.comptesjustes.ui.transaction;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import be.helha.comptesjustes.R;
import be.helha.comptesjustes.dto.transaction.DtoCreateTransaction;
import be.helha.comptesjustes.dto.transaction.DtoTransaction;
import be.helha.comptesjustes.dto.transactionaccount.DtoCreateTransactionAccount;
import be.helha.comptesjustes.infrastructure.ITransactionAccountRepository;
import be.helha.comptesjustes.infrastructure.ITransactionRepository;
import be.helha.comptesjustes.infrastructure.Retrofit;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link TransactionCreateFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class TransactionCreateFragment extends Fragment
{

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_ACCOUNT_ID = "account-id";
    private static final String ARG_USER_ID = "user-id";

    // TODO: Rename and change types of parameters
    private int accountId;
    private int userId;

    public TransactionCreateFragment()
    {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param accountId Parameter 1.
     * @param userId Parameter 2.
     * @return A new instance of fragment TransactionCreateFragment.
     */
    public static TransactionCreateFragment newInstance( int accountId, int userId )
    {
        TransactionCreateFragment fragment = new TransactionCreateFragment();
        Bundle args = new Bundle();
        args.putInt( ARG_ACCOUNT_ID, accountId );
        args.putInt( ARG_USER_ID, userId );
        fragment.setArguments( args );
        return fragment;
    }

    @Override
    public void onCreate( Bundle savedInstanceState )
    {
        super.onCreate( savedInstanceState );
        if ( getArguments() != null )
        {
            accountId = getArguments().getInt( ARG_ACCOUNT_ID );
            userId = getArguments().getInt( ARG_USER_ID );
        }
    }

    @Override
    public View onCreateView( LayoutInflater inflater, ViewGroup container,
                              Bundle savedInstanceState )
    {
        Log.d( "Account ID", String.valueOf( accountId ) );
        View view = inflater.inflate( R.layout.fragment_transaction_create, container, false );

        EditText etAmount = view.findViewById( R.id.et_fragmentTransactionCreate_amount );
        DatePicker dpDate = view.findViewById( R.id.dp_fragmentTransactionCreate_date );
        EditText etCommunication = view.findViewById( R.id.et_fragmentTransactionCreate_communication );
        EditText etLevel = view.findViewById( R.id.et_fragmentTransactionCreate_necessityLevel );

        Button btnSubmit = view.findViewById( R.id.btn_fragmentTransactionCreate_submit );

        btnSubmit.setOnClickListener( new View.OnClickListener()
        {
            @Override
            public void onClick( View view )
            {
                try
                {
                    boolean emptyString = false;
                    double roundedAmount = Math.round( Double.parseDouble( etAmount.getText().toString() ) * 100.0 ) / 100.0;

                    SimpleDateFormat simpleDateFormat = new SimpleDateFormat( "dd/MM/yyyy" );
                    Calendar cal = new GregorianCalendar( dpDate.getYear(), dpDate.getMonth(), dpDate.getDayOfMonth() );
                    Date d = new Date( cal.getTimeInMillis() );
                    String strDate = simpleDateFormat.format( d );

                    String strCommunication = etCommunication.getText().toString();

                    int level = Integer.parseInt( etLevel.getText().toString() );

                    String[] strings =
                            {
                                    strDate,
                                    strCommunication
                            };

                    for( String s : strings )
                    {
                        if ( s.isEmpty() )
                        {
                            emptyString = true;
                            break;
                        }
                    }

                    if( emptyString )
                    {
                        Log.e( "Error", "Empty Input Not Allowed" );
                        Toast.makeText( getContext(), "Empty Input Not Allowed", Toast.LENGTH_LONG ).show();
                    }
                    else
                    {
                        DtoCreateTransaction createdTransaction = new DtoCreateTransaction( roundedAmount, strDate, level, strCommunication );

                        Retrofit.getInstance().create( ITransactionRepository.class ).create( createdTransaction ).enqueue( new Callback< DtoTransaction >()
                        {
                            @Override
                            public void onResponse( @NonNull Call< DtoTransaction > call, @NonNull Response< DtoTransaction > response )
                            {
                                Log.d( "Created Transaction", createdTransaction.toString() );
                                assert response.body() != null;
                                DtoCreateTransactionAccount dtoCreateTransactionAccount = new DtoCreateTransactionAccount( accountId, response.body().getId() );
                                Log.d( "Account-Transaction", dtoCreateTransactionAccount.toString() );
                                Retrofit.getInstance().create( ITransactionAccountRepository.class ).create( dtoCreateTransactionAccount ).enqueue( new Callback< DtoCreateTransactionAccount >()
                                {
                                    @Override
                                    public void onResponse( @NonNull Call< DtoCreateTransactionAccount > call, @NonNull Response< DtoCreateTransactionAccount > response )
                                    {
                                        Log.d( "Created Bond", String.valueOf( response.body() ) );
                                    }

                                    @Override
                                    public void onFailure( @NonNull Call< DtoCreateTransactionAccount > call, @NonNull Throwable t )
                                    {
                                        Log.e( "Error", t.toString() );
                                    }
                                } );
                            }

                            @Override
                            public void onFailure( @NonNull Call< DtoTransaction > call, @NonNull Throwable t )
                            {
                                Log.e( "Error", t.toString() );
                            }
                        } );
                    }
                }
                catch ( Exception e )
                {
                    Toast.makeText( getContext(), e.getMessage(), Toast.LENGTH_SHORT ).show();
                    e.printStackTrace();
                }
                FragmentManager fragmentManager = getParentFragmentManager();
                FragmentTransaction transaction = fragmentManager.beginTransaction();
                transaction.commit();
                fragmentManager.popBackStack();
            }
        } );

        return view;
    }
}
