package be.helha.comptesjustes.ui.account;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.dialog.MaterialAlertDialogBuilder;

import be.helha.comptesjustes.databinding.FragmentAccountItemBinding;
import be.helha.comptesjustes.dto.account.DtoAccount;
import be.helha.comptesjustes.infrastructure.IAccountUserRepository;
import be.helha.comptesjustes.infrastructure.Retrofit;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import java.util.List;

/**
 * {@link RecyclerView.Adapter} that can display a {@link be.helha.comptesjustes.dto.account.DtoAccount}.
 * TODO: Replace the implementation with code for your data type.
 */
public class AccountListRecyclerViewAdapter extends RecyclerView.Adapter< AccountListRecyclerViewAdapter.ViewHolder >
{

    private final List< DtoAccount > mValues;
    private final AccountListFragment.OnAccountClickListener accountClickListener;
    private Context context;

    public AccountListRecyclerViewAdapter( List< DtoAccount > items, AccountListFragment.OnAccountClickListener accountClickListener, Context context )
    {
        mValues = items;
        this.accountClickListener = accountClickListener;
        this.context = context;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder( @NonNull ViewGroup parent, int viewType )
    {

        return new ViewHolder( FragmentAccountItemBinding.inflate( LayoutInflater.from( parent.getContext() ), parent, false ) );

    }

    @Override
    public void onBindViewHolder( final ViewHolder holder, int position )
    {
        final DtoAccount dtoAccount = mValues.get( position );
        holder.mItem = dtoAccount;
        holder.tvId.setText( String.valueOf( dtoAccount.getId() ) );
        holder.tvName.setText( dtoAccount.getName() );
        holder.bind( accountClickListener );
    }

    @Override
    public int getItemCount()
    {
        return mValues.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder
    {
        public final TextView tvId;
        public final TextView tvName;
        public DtoAccount mItem;

        public ViewHolder( FragmentAccountItemBinding binding )
        {
            super( binding.getRoot() );
            tvId = binding.tvFragmentAccountListId;
            tvName = binding.tvFragmentAccountListName;
        }

        public void bind( AccountListFragment.OnAccountClickListener onAccountClickListener )
        {
            itemView.setOnClickListener( v -> onAccountClickListener.onAccountClick( mItem ) );
            itemView.setOnLongClickListener( new View.OnLongClickListener()
            {
                @Override
                public boolean onLongClick( View v )
                {
                    MaterialAlertDialogBuilder builder = new MaterialAlertDialogBuilder( v.getContext() );
                    builder.setTitle( "Confirm Removal" )
                            .setMessage( "Are you sure you want to remove yourself from the account?" )
                            .setPositiveButton( "Yes", new DialogInterface.OnClickListener()
                            {
                                @Override
                                public void onClick( DialogInterface dialog, int which )
                                {
                                    SharedPreferences preferences = context.getSharedPreferences( "user", Context.MODE_PRIVATE );
                                    int userId = preferences.getInt( "userId", 0 );
                                    Retrofit.getInstance().create( IAccountUserRepository.class ).delete( userId, mItem.getId() ).enqueue( new Callback< Void >()
                                    {
                                        @Override
                                        public void onResponse( @NonNull Call< Void > call, @NonNull Response< Void > response )
                                        {
                                            Log.d( "Deletion", "Successful" );
                                            Toast.makeText( context.getApplicationContext(), "Account left successfully, please refresh the page", Toast.LENGTH_SHORT ).show();
                                        }

                                        @Override
                                        public void onFailure( @NonNull Call< Void > call, @NonNull Throwable t )
                                        {
                                            Log.e( "Error", t.toString() );
                                        }
                                    } );
                                }
                            } )
                            .setNegativeButton( "No", null )
                            .show();
                    return false;
                }
            } );
        }

        @Override
        public String toString()
        {
            return super.toString() + " '" + tvName.getText() + "'";
        }
    }
}
