package be.helha.comptesjustes.ui.profile;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import java.util.Objects;

import be.helha.comptesjustes.R;
import be.helha.comptesjustes.dto.user.DtoCreateUser;
import be.helha.comptesjustes.dto.user.DtoUser;
import be.helha.comptesjustes.infrastructure.IUserRepository;
import be.helha.comptesjustes.infrastructure.Retrofit;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link ProfileFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ProfileFragment extends Fragment
{

    private static final String ARG_USER_ID = "param1";
    private EditText etUsername, etFirstName, etLastName, etEmailAddress, etAccountNumber, etPassword;
    private Spinner sAccountNumberPrefix;
    private Button btnSubmit;

    private int userId;

    public ProfileFragment()
    {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param id Parameter 1.
     * @return A new instance of fragment ProfileFragment.
     */
    public static ProfileFragment newInstance( int id )
    {
        ProfileFragment fragment = new ProfileFragment();
        Bundle args = new Bundle();
        args.putInt( ARG_USER_ID, id );
        fragment.setArguments( args );
        return fragment;
    }

    @Override
    public void onCreate( Bundle savedInstanceState )
    {
        super.onCreate( savedInstanceState );
        if ( getArguments() != null )
        {
            userId = getArguments().getInt( ARG_USER_ID );
        }
    }

    @Override
    public View onCreateView( LayoutInflater inflater, ViewGroup container,
                              Bundle savedInstanceState )
    {
        // Inflate the layout for this fragment
        View view = inflater.inflate( R.layout.fragment_profile, container, false );

        AppCompatActivity activity = ( ( AppCompatActivity ) getActivity() );
        assert activity != null;
        Objects.requireNonNull( activity.getSupportActionBar() ).setTitle(
                getResources().getString( R.string.bottomNavBar_mainActivity_profile ) );

        setHasOptionsMenu( true );

        initViewInstances( view );
        initListeners();

        return view;
    }

    private void initViewInstances( View view )
    {
        etUsername = view.findViewById( R.id.et_profileFragment_username );
        etFirstName = view.findViewById( R.id.et_profileFragment_firstName );
        etLastName = view.findViewById( R.id.et_profileFragment_lastName );
        etEmailAddress = view.findViewById( R.id.et_profileFragment_emailAddress );
        etAccountNumber = view.findViewById( R.id.et_profileFragment_accountNumber );
        etPassword = view.findViewById( R.id.et_profileFragment_password );
        sAccountNumberPrefix = view.findViewById( R.id.s_profileFragment_accountNumberPrefix );
        btnSubmit = view.findViewById( R.id.btn_profileFragment_submit );
        Retrofit.getInstance().create( IUserRepository.class ).getById( userId ).enqueue( new Callback< DtoUser >()
        {
            @Override
            public void onResponse( @NonNull Call< DtoUser > call, @NonNull Response< DtoUser > response )
            {
                DtoUser dtoUser = response.body();
                assert dtoUser != null;
                etUsername.setText( dtoUser.getNickname() );
                etFirstName.setText( dtoUser.getFirstName() );
                etLastName.setText( dtoUser.getLastName() );
                etEmailAddress.setText( dtoUser.getEmail() );
                etAccountNumber.setText( dtoUser.getAccountNumber().substring( 2 ) );
                etPassword.setText( dtoUser.getPassword() );
            }

            @Override
            public void onFailure( @NonNull Call< DtoUser > call, @NonNull Throwable t )
            {
                Log.e( "Error", t.toString() );
            }
        } );
    }

    private void initListeners()
    {
        ArrayAdapter< CharSequence > spinnerAdapter = ArrayAdapter.createFromResource( getContext(), R.array.account_prefixes, android.R.layout.simple_spinner_item );
        spinnerAdapter.setDropDownViewResource( android.R.layout.simple_spinner_dropdown_item );
        sAccountNumberPrefix.setAdapter( spinnerAdapter );

        btnSubmit.setOnClickListener( new View.OnClickListener()
        {
            @Override
            public void onClick( View v )
            {
                boolean emptyString = false;
                String strUsername = etUsername.getText().toString();
                String strFirstName = etFirstName.getText().toString();
                String strLastName = etLastName.getText().toString();
                String strEmailAddress = etEmailAddress.getText().toString();
                String strAccountNumber = sAccountNumberPrefix.getSelectedItem().toString() + etAccountNumber.getText().toString().toUpperCase();
                String strPassword = etPassword.getText().toString();

                String[] strings =
                        {
                                strUsername,
                                strFirstName,
                                strLastName,
                                strEmailAddress,
                                strAccountNumber,
                                strPassword
                        };

                for( String s : strings )
                {
                    if( s.isEmpty() )
                    {
                        emptyString = true;
                        break;
                    }
                }

                if( emptyString )
                {
                    Log.e( "Error", "Empty Input Not Allowed" );
                    Toast.makeText( getContext(), "Empty Input Not Allowed", Toast.LENGTH_LONG ).show();
                }
                else if( strPassword.length() < 8 )
                {
                    Log.e( "Error", "Password less than 8 characters" );
                    Toast.makeText( getContext(), "Password less than 8 characters", Toast.LENGTH_SHORT ).show();
                }
                else if( !strEmailAddress.contains( "@" ) ||
                        !strEmailAddress.split( "@" )[1].contains( "." ) ||
                        strEmailAddress.split( "@" ).length > 2 )
                {
                    Log.e( "Error", "Not an email address" );
                    Toast.makeText( getContext(), "Not an email address", Toast.LENGTH_SHORT ).show();
                }
                else
                {
                    DtoUser dtoUser = new DtoUser( userId, strFirstName, strLastName, strEmailAddress, strUsername, strPassword, strAccountNumber );
                    Retrofit.getInstance().create( IUserRepository.class ).update( dtoUser, userId ).enqueue( new Callback< Void >()
                    {
                        @Override
                        public void onResponse( @NonNull Call< Void > call, @NonNull Response< Void > response )
                        {
                            Log.d( "User Update", "Success" );
                            Toast.makeText( getContext(), "Information Update Successfully", Toast.LENGTH_SHORT ).show();
                        }

                        @Override
                        public void onFailure( @NonNull Call< Void > call, @NonNull Throwable t )
                        {
                            Log.e( "Error", t.toString() );
                        }
                    } );
                }
            }
        } );
    }
}
