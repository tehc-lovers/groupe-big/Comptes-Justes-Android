package be.helha.comptesjustes.ui.login;

import android.util.Log;

import java.io.IOException;

import be.helha.comptesjustes.dto.user.DtoUser;

/**
 * Class that handles authentication w/ login credentials and retrieves user information.
 */
public class LoginDataSource
{
    public Result< DtoUser > login( DtoUser dtoUser )
    {
        try
        {
            Log.d( "Logged in user", String.valueOf( dtoUser ) );
            if( dtoUser == null )
            {
                throw new Exception( "Null User, Login failed" );
            }
            return new Result.Success< DtoUser >( dtoUser );
        } catch ( Exception e )
        {
            return new Result.Error( new IOException( "Error logging in", e ) );
        }
    }

    public void logout()
    {
        // TODO: revoke authentication
    }
}
