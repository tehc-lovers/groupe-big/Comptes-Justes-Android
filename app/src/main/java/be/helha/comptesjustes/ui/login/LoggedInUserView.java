package be.helha.comptesjustes.ui.login;

import be.helha.comptesjustes.dto.user.DtoUser;

/**
 * Class exposing authenticated user details to the UI.
 */
public class LoggedInUserView
{
    private int id;
    private String firstName;
    private String lastName;
    private String email;
    private String nickname;
    private String accountNumber;
    //... other data fields that may be accessible to the UI

    public LoggedInUserView( DtoUser dtoUser )
    {
        id = dtoUser.getId();
        firstName = dtoUser.getFirstName();
        lastName = dtoUser.getLastName();
        email = dtoUser.getEmail();
        nickname = dtoUser.getNickname();
        accountNumber = dtoUser.getAccountNumber();
    }

    public int getId()
    {
        return id;
    }

    public void setId( int id )
    {
        this.id = id;
    }

    public String getFirstName()
    {
        return firstName;
    }

    public void setFirstName( String firstName )
    {
        this.firstName = firstName;
    }

    public String getLastName()
    {
        return lastName;
    }

    public void setLastName( String lastName )
    {
        this.lastName = lastName;
    }

    public String getEmail()
    {
        return email;
    }

    public void setEmail( String email )
    {
        this.email = email;
    }

    public String getNickname()
    {
        return nickname;
    }

    public void setNickname( String nickname )
    {
        this.nickname = nickname;
    }

    public String getAccountNumber()
    {
        return accountNumber;
    }

    public void setAccountNumber( String accountNumber )
    {
        this.accountNumber = accountNumber;
    }
}
