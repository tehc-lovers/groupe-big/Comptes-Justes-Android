package be.helha.comptesjustes.ui.login;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import android.util.Patterns;

import be.helha.comptesjustes.R;
import be.helha.comptesjustes.dto.user.DtoUser;

public class LoginViewModel extends ViewModel
{

    private MutableLiveData< LoginFormState > loginFormState = new MutableLiveData<>();
    private MutableLiveData< LoginResult > loginResult = new MutableLiveData<>();
    private LoginRepository loginRepository;

    public LoginViewModel( LoginRepository loginRepository )
    {
        this.loginRepository = loginRepository;
    }

    public LiveData< LoginFormState > getLoginFormState()
    {
        return loginFormState;
    }

    public LiveData< LoginResult > getLoginResult()
    {
        return loginResult;
    }

    public void login( DtoUser dtoUser )
    {
        // can be launched in a separate asynchronous job
        Result< DtoUser > result = loginRepository.login( dtoUser );

        if ( result instanceof Result.Success )
        {
            DtoUser data = ( ( Result.Success< DtoUser > ) result ).getData();
            loginResult.setValue( new LoginResult( new LoggedInUserView( data ) ) );
        } else
        {
            loginResult.setValue( new LoginResult( R.string.login_failed ) );
        }
    }

    public void loginDataChanged( String username, String password )
    {
        if ( !isUserNameValid( username ) )
        {
            loginFormState.setValue( new LoginFormState( R.string.invalid_username, null ) );
        } else if ( !isPasswordValid( password ) )
        {
            loginFormState.setValue( new LoginFormState( null, R.string.invalid_password ) );
        } else
        {
            loginFormState.setValue( new LoginFormState( true ) );
        }
    }

    // A placeholder username validation check
    private boolean isUserNameValid( String username )
    {
        if ( username == null )
        {
            return false;
        }
        if ( username.contains( "@" ) )
        {
            return Patterns.EMAIL_ADDRESS.matcher( username ).matches();
        } else
        {
            return !username.trim().isEmpty();
        }
    }

    // A placeholder password validation check
    private boolean isPasswordValid( String password )
    {
        return password != null && password.trim().length() > 7;
    }
}
