package be.helha.comptesjustes.domain;

import android.util.Base64;

import java.nio.charset.StandardCharsets;

import javax.crypto.SecretKey;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jws;
import io.jsonwebtoken.JwtException;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.security.Keys;

public class KeyHandler
{
    public KeyHandler()
    {
    }

    public String createJsonWebToken( String email, byte[] password ) throws JwtException
    {
        SecretKey key = Keys.hmacShaKeyFor( password );

        return Jwts.builder().setSubject( email ).signWith( key ).compact();
    }

    public Jws< Claims > retrieveJsonWebTokenInformation( SecretKey key, String jws ) throws JwtException
    {
        return Jwts.parserBuilder().setSigningKey( key ).build().parseClaimsJws( jws );
    }
}
