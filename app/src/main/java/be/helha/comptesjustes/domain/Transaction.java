package be.helha.comptesjustes.domain;

import java.time.LocalDateTime;
import java.util.Objects;

public class Transaction
{
    private int id;
    private double amount;
    private LocalDateTime date;
    private int level;
    private String communication;

    public Transaction( double amount, LocalDateTime date, int level, String communication )
    {
        this.amount = amount;
        this.date = date;
        this.level = level;
        this.communication = communication;
    }

    public double getAmount()
    {
        return amount;
    }

    public void setAmount( double amount )
    {
        this.amount = amount;
    }

    public LocalDateTime getDate()
    {
        return date;
    }

    public void setDate( LocalDateTime date )
    {
        this.date = date;
    }

    public int getLevel()
    {
        return level;
    }

    public void setLevel( int level )
    {
        this.level = level;
    }

    public String getCommunication()
    {
        return communication;
    }

    public void setCommunication( String communication )
    {
        this.communication = communication;
    }

    @Override
    public String toString()
    {
        return "Transaction{" +
                "id=" + id +
                ", amount=" + amount +
                ", date=" + date +
                ", necessityLevel=" + level +
                ", communication='" + communication + '\'' +
                '}';
    }

    @Override
    public boolean equals( Object o )
    {
        if ( this == o ) return true;
        if ( o == null || getClass() != o.getClass() ) return false;
        Transaction that = ( Transaction ) o;
        return id == that.id;
    }

    @Override
    public int hashCode()
    {
        return Objects.hash( id );
    }
}
