package be.helha.comptesjustes.domain;

import java.util.Objects;

public class Account
{
    private int id;
    private String name;

    public Account( String name )
    {
        this.name = name;
    }

    public String getName()
    {
        return name;
    }

    public void setName( String name )
    {
        this.name = name;
    }

    @Override
    public String toString()
    {
        return "Account{" +
                "id=" + id +
                ", name='" + name + '\'' +
                '}';
    }

    @Override
    public boolean equals( Object o )
    {
        if ( this == o ) return true;
        if ( o == null || getClass() != o.getClass() ) return false;
        Account account = ( Account ) o;
        return id == account.id;
    }

    @Override
    public int hashCode()
    {
        return Objects.hash( id );
    }
}
