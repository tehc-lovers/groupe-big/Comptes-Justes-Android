package be.helha.comptesjustes.dto.account;

public class DtoCreateAccount
{
    private String name;

    public DtoCreateAccount( String name )
    {
        this.name = name;
    }

    public String getName()
    {
        return name;
    }

    public void setName( String name )
    {
        this.name = name;
    }
}
