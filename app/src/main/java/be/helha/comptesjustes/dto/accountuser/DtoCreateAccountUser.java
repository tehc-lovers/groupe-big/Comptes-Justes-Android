package be.helha.comptesjustes.dto.accountuser;

public class DtoCreateAccountUser
{
    private int idAccount;
    private int idUser;

    public DtoCreateAccountUser( int idAccount, int idUser )
    {
        this.idAccount = idAccount;
        this.idUser = idUser;
    }

    public int getIdAccount()
    {
        return idAccount;
    }

    public void setIdAccount( int idAccount )
    {
        this.idAccount = idAccount;
    }

    public int getIdUser()
    {
        return idUser;
    }

    public void setIdUser( int idUser )
    {
        this.idUser = idUser;
    }

    @Override
    public String toString()
    {
        return "DtoCreateAccountUser{" +
                "idAccount=" + idAccount +
                ", idUser=" + idUser +
                '}';
    }
}
