package be.helha.comptesjustes.dto.transaction;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.Objects;

public class DtoTransaction implements Parcelable
{
    private int id;
    private double amount;
    private String date;
    private int level;
    private String communication;

    public DtoTransaction( int id, double amount, String date, int level, String communication )
    {
        this.id = id;
        this.amount = amount;
        this.date = date;
        this.level = level;
        this.communication = communication;
    }

    protected DtoTransaction( Parcel in )
    {
        id = in.readInt();
        amount = in.readDouble();
        level = in.readInt();
        communication = in.readString();
    }

    public static final Creator< DtoTransaction > CREATOR = new Creator< DtoTransaction >()
    {
        @Override
        public DtoTransaction createFromParcel( Parcel in )
        {
            return new DtoTransaction( in );
        }

        @Override
        public DtoTransaction[] newArray( int size )
        {
            return new DtoTransaction[ size ];
        }
    };

    public int getId()
    {
        return id;
    }

    public void setId( int id )
    {
        this.id = id;
    }

    public double getAmount()
    {
        return amount;
    }

    public void setAmount( double amount )
    {
        this.amount = amount;
    }

    public String getDate()
    {
        return date;
    }

    public void setDate( String date )
    {
        this.date = date;
    }

    public int getLevel()
    {
        return level;
    }

    public void setLevel( int level )
    {
        this.level = level;
    }

    public String getCommunication()
    {
        return communication;
    }

    public void setCommunication( String communication )
    {
        this.communication = communication;
    }

    @Override
    public String toString()
    {
        return "DtoTransaction{" +
                "id=" + id +
                ", amount=" + amount +
                ", date=" + date +
                ", necessityLevel=" + level +
                ", communication='" + communication + '\'' +
                '}';
    }

    @Override
    public boolean equals( Object o )
    {
        if ( this == o ) return true;
        if ( o == null || getClass() != o.getClass() ) return false;
        DtoTransaction that = ( DtoTransaction ) o;
        return id == that.id;
    }

    @Override
    public int hashCode()
    {
        return Objects.hash( id );
    }

    @Override
    public int describeContents()
    {
        return 0;
    }

    @Override
    public void writeToParcel( Parcel dest, int flags )
    {
        dest.writeInt( id );
        dest.writeDouble( amount );
        dest.writeInt( level );
        dest.writeString( communication );
    }
}
