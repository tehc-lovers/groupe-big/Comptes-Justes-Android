package be.helha.comptesjustes.dto.transactionaccount;

public class DtoCreateTransactionAccount
{
    private int idAccount;
    private int idTransaction;

    public DtoCreateTransactionAccount( int idAccount, int idTransaction )
    {
        this.idAccount = idAccount;
        this.idTransaction = idTransaction;
    }

    public int getIdAccount()
    {
        return idAccount;
    }

    public void setIdAccount( int idAccount )
    {
        this.idAccount = idAccount;
    }

    public int getIdTransaction()
    {
        return idTransaction;
    }

    public void setIdTransaction( int idTransaction )
    {
        this.idTransaction = idTransaction;
    }

    @Override
    public String toString()
    {
        return "DtoCreateTransactionAccount{" +
                "idAccount=" + idAccount +
                ", idTransaction=" + idTransaction +
                '}';
    }
}
