package be.helha.comptesjustes.dto.user;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.Objects;

public class DtoUser implements Parcelable
{
    public static final Creator< DtoUser > CREATOR = new Creator< DtoUser >()
    {
        @Override
        public DtoUser createFromParcel( Parcel in )
        {
            return new DtoUser( in );
        }

        @Override
        public DtoUser[] newArray( int size )
        {
            return new DtoUser[ size ];
        }
    };

    private int id;
    private String firstName;
    private String lastName;
    private String email;
    private String nickname;
    private String password;
    private String accountNumber;

    public DtoUser( int id, String firstName, String lastName, String email, String nickname, String password, String accountNumber )
    {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.nickname = nickname;
        this.password = password;
        this.accountNumber = accountNumber;
    }

    protected DtoUser( Parcel in )
    {
        id = in.readInt();
        firstName = in.readString();
        lastName = in.readString();
        email = in.readString();
        nickname = in.readString();
        password = in.readString();
        accountNumber = in.readString();
    }

    public int getId()
    {
        return id;
    }

    public void setId( int id )
    {
        this.id = id;
    }

    public String getFirstName()
    {
        return firstName;
    }

    public void setFirstName( String firstName )
    {
        this.firstName = firstName;
    }

    public String getLastName()
    {
        return lastName;
    }

    public void setLastName( String lastName )
    {
        this.lastName = lastName;
    }

    public String getEmail()
    {
        return email;
    }

    public void setEmail( String email )
    {
        this.email = email;
    }

    public String getNickname()
    {
        return nickname;
    }

    public void setNickname( String nickname )
    {
        this.nickname = nickname;
    }

    public String getPassword()
    {
        return password;
    }

    public void setPassword( String password )
    {
        this.password = password;
    }

    public String getAccountNumber()
    {
        return accountNumber;
    }

    public void setAccountNumber( String accountNumber )
    {
        this.accountNumber = accountNumber;
    }

    @Override
    public String toString()
    {
        return "DtoUser{" +
                "id=" + id +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", email='" + email + '\'' +
                ", nickname='" + nickname + '\'' +
                ", password='" + password + '\'' +
                ", accountNumber='" + accountNumber + '\'' +
                '}';
    }

    @Override
    public boolean equals( Object o )
    {
        if ( this == o ) return true;
        if ( o == null || getClass() != o.getClass() ) return false;
        DtoUser dtoUser = ( DtoUser ) o;
        return id == dtoUser.id;
    }

    @Override
    public int hashCode()
    {
        return Objects.hash( id );
    }

    @Override
    public int describeContents()
    {
        return 0;
    }

    @Override
    public void writeToParcel( Parcel dest, int flags )
    {
        dest.writeInt( id );
        dest.writeString( firstName );
        dest.writeString( lastName );
        dest.writeString( email );
        dest.writeString( nickname );
        dest.writeString( password );
        dest.writeString( accountNumber );
    }
}
