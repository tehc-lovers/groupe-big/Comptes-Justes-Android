package be.helha.comptesjustes.dto.accountmanager;

public class DtoCreateAccountManager
{
    private int idAccount;
    private int idManager;

    public DtoCreateAccountManager( int idAccount, int idManager )
    {
        this.idAccount = idAccount;
        this.idManager = idManager;
    }

    public int getIdAccount()
    {
        return idAccount;
    }

    public void setIdAccount( int idAccount )
    {
        this.idAccount = idAccount;
    }

    public int getIdManager()
    {
        return idManager;
    }

    public void setIdManager( int idManager )
    {
        this.idManager = idManager;
    }

    @Override
    public String toString()
    {
        return "DtoCreateAccountManager{" +
                "idAccount=" + idAccount +
                ", idManager=" + idManager +
                '}';
    }
}
