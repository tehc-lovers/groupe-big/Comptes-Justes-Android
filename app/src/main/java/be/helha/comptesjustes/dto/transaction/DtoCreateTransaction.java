package be.helha.comptesjustes.dto.transaction;

public class DtoCreateTransaction
{
    private double amount;
    private String date;
    private int level;
    private String communication;

    public DtoCreateTransaction( double amount, String date, int level, String communication )
    {
        this.amount = amount;
        this.date = date;
        this.level = level;
        this.communication = communication;
    }

    public double getAmount()
    {
        return amount;
    }

    public void setAmount( double amount )
    {
        this.amount = amount;
    }

    public String getDate()
    {
        return date;
    }

    public void setDate( String date )
    {
        this.date = date;
    }

    public int getLevel()
    {
        return level;
    }

    public void setLevel( int level )
    {
        this.level = level;
    }

    public String getCommunication()
    {
        return communication;
    }

    public void setCommunication( String communication )
    {
        this.communication = communication;
    }

    @Override
    public String toString()
    {
        return "DtoCreateTransaction{" +
                "amount=" + amount +
                ", date='" + date + '\'' +
                ", necessityLevel=" + level +
                ", communication='" + communication + '\'' +
                '}';
    }
}
