package be.helha.comptesjustes.dto.account;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.Objects;

public class DtoAccount implements Parcelable
{
    private int id;
    private String name;

    public DtoAccount( int id, String name )
    {
        this.id = id;
        this.name = name;
    }

    protected DtoAccount( Parcel in )
    {
        id = in.readInt();
        name = in.readString();
    }

    public static final Creator< DtoAccount > CREATOR = new Creator< DtoAccount >()
    {
        @Override
        public DtoAccount createFromParcel( Parcel in )
        {
            return new DtoAccount( in );
        }

        @Override
        public DtoAccount[] newArray( int size )
        {
            return new DtoAccount[ size ];
        }
    };

    public int getId()
    {
        return id;
    }

    public void setId( int id )
    {
        this.id = id;
    }

    public String getName()
    {
        return name;
    }

    public void setName( String name )
    {
        this.name = name;
    }

    @Override
    public String toString()
    {
        return "DtoAccount{" +
                "id=" + id +
                ", name='" + name + '\'' +
                '}';
    }

    @Override
    public boolean equals( Object o )
    {
        if ( this == o ) return true;
        if ( o == null || getClass() != o.getClass() ) return false;
        DtoAccount that = ( DtoAccount ) o;
        return id == that.id;
    }

    @Override
    public int hashCode()
    {
        return Objects.hash( id );
    }

    @Override
    public int describeContents()
    {
        return 0;
    }

    @Override
    public void writeToParcel( Parcel dest, int flags )
    {
        dest.writeInt( id );
        dest.writeString( name );
    }
}
