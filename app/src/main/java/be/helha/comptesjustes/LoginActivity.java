package be.helha.comptesjustes;

import android.app.Activity;

import androidx.annotation.NonNull;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.annotation.StringRes;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Handler;
import android.os.Looper;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

import be.helha.comptesjustes.databinding.ActivityLoginBinding;
import be.helha.comptesjustes.dto.user.DtoUser;
import be.helha.comptesjustes.infrastructure.IUserRepository;
import be.helha.comptesjustes.infrastructure.Retrofit;
import be.helha.comptesjustes.ui.login.LoggedInUserView;
import be.helha.comptesjustes.ui.login.LoginFormState;
import be.helha.comptesjustes.ui.login.LoginResult;
import be.helha.comptesjustes.ui.login.LoginViewModel;
import be.helha.comptesjustes.ui.login.LoginViewModelFactory;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginActivity extends AppCompatActivity
{
    private static final int REQ_CODE = 1;
    private LoginViewModel loginViewModel;
    private ActivityLoginBinding binding;
    private SharedPreferences preferences;
    private DtoUser user;

    @Override
    public void onCreate( Bundle savedInstanceState )
    {
        super.onCreate( savedInstanceState );

        binding = ActivityLoginBinding.inflate( getLayoutInflater() );
        setContentView( binding.getRoot() );

        loginViewModel = new ViewModelProvider( this, new LoginViewModelFactory() )
                .get( LoginViewModel.class );

        final EditText usernameEditText = binding.etLoginActivityUserName;
        final EditText passwordEditText = binding.etLoginActivityPassword;
        final Button loginButton = binding.btnLoginActivityLogin;
        final Button registerButton = binding.btnLoginActivityRegister;
        final ProgressBar loadingProgressBar = binding.pbLoginActivityLoading;

        loginViewModel.getLoginFormState().observe( this, new Observer< LoginFormState >()
        {
            @Override
            public void onChanged( @Nullable LoginFormState loginFormState )
            {
                if ( loginFormState == null )
                {
                    return;
                }
                loginButton.setEnabled( loginFormState.isDataValid() );
                if ( loginFormState.getUsernameError() != null )
                {
                    usernameEditText.setError( getString( loginFormState.getUsernameError() ) );
                }
                if ( loginFormState.getPasswordError() != null )
                {
                    passwordEditText.setError( getString( loginFormState.getPasswordError() ) );
                }
            }
        } );

        loginViewModel.getLoginResult().observe( this, new Observer< LoginResult >()
        {
            @Override
            public void onChanged( @Nullable LoginResult loginResult )
            {
                if ( loginResult == null )
                {
                    return;
                }
                loadingProgressBar.setVisibility( View.GONE );
                if ( loginResult.getError() != null )
                {
                    showLoginFailed( loginResult.getError() );
                }
                if ( loginResult.getSuccess() != null )
                {
                    updateUiWithUser( loginResult.getSuccess() );
                }
                setResult( Activity.RESULT_OK );
                //Complete and destroy login activity once successful
                finish();
            }
        } );

        TextWatcher afterTextChangedListener = new TextWatcher()
        {
            @Override
            public void beforeTextChanged( CharSequence s, int start, int count, int after )
            {
                // ignore
            }

            @Override
            public void onTextChanged( CharSequence s, int start, int before, int count )
            {
                // ignore
            }

            @Override
            public void afterTextChanged( Editable s )
            {
                loginViewModel.loginDataChanged( usernameEditText.getText().toString(),
                        passwordEditText.getText().toString() );
            }
        };
        usernameEditText.addTextChangedListener( afterTextChangedListener );
        passwordEditText.addTextChangedListener( afterTextChangedListener );
        /*passwordEditText.setOnEditorActionListener( new TextView.OnEditorActionListener()
        {

            @Override
            public boolean onEditorAction( TextView v, int actionId, KeyEvent event )
            {
                if ( actionId == EditorInfo.IME_ACTION_DONE )
                {
                    loginViewModel.login( usernameEditText.getText().toString(),
                            passwordEditText.getText().toString() );
                }
                return false;
            }
        } );*/

        loginButton.setOnClickListener( new View.OnClickListener()
        {
            @Override
            public void onClick( View v )
            {
                String username = usernameEditText.getText().toString();
                String password = passwordEditText.getText().toString();
                Retrofit.getInstance().create( IUserRepository.class ).getAll().enqueue( new Callback< List< DtoUser > >()
                {
                    @Override
                    public void onResponse( @NonNull Call< List< DtoUser > > call, @NonNull Response< List< DtoUser > > response )
                    {
                        assert response.body() != null;
                        Log.d( "User List", String.valueOf( response.body() ) );
                        for( DtoUser dtoUser : response.body() )
                        {
                            if ( dtoUser.getNickname().equalsIgnoreCase( username ) && dtoUser.getPassword().equals( password ) )
                            {
                                user = dtoUser;
                                Log.d( "User Found", String.valueOf( user ) );
                                break;
                            }
                        }
                    }

                    @Override
                    public void onFailure( @NonNull Call< List< DtoUser > > call, @NonNull Throwable t )
                    {
                        Log.e( "Error", t.toString() );
                    }
                } );
                loadingProgressBar.setVisibility( View.VISIBLE );
                final Handler handler = new Handler( Looper.getMainLooper() );
                handler.postDelayed( new Runnable()
                {
                    @Override
                    public void run()
                    {
                        loginViewModel.login( user );
                    }
                }, 1000 );
            }
        } );

        registerButton.setOnClickListener( new View.OnClickListener()
        {
            @Override
            public void onClick( View view )
            {
                Intent registerIntent = new Intent( LoginActivity.this, RegisterActivity.class );
                startActivity( registerIntent );
            }
        } );
    }

    private void updateUiWithUser( LoggedInUserView model )
    {
        String welcome = getString( R.string.welcome ) + model.getNickname();
        preferences = getSharedPreferences( "user", Context.MODE_PRIVATE );
        preferences.edit().putInt( "userId", model.getId() ).apply();
        preferences.edit().putString( "userNickname", model.getNickname() ).apply();
        preferences.edit().putString( "userFirstName", model.getFirstName() ).apply();
        preferences.edit().putString( "userLastName", model.getLastName() ).apply();
        preferences.edit().putString( "userEmail", model.getEmail() ).apply();
        preferences.edit().putString( "userAccountNumber", model.getAccountNumber() ).apply();
        // TODO : initiate successful logged in experience
        Toast.makeText( getApplicationContext(), welcome, Toast.LENGTH_LONG ).show();

    }

    private void showLoginFailed( @StringRes Integer errorString )
    {
        Toast.makeText( getApplicationContext(), errorString, Toast.LENGTH_SHORT ).show();
    }
}
