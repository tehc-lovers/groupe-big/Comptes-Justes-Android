package be.helha.comptesjustes;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.dialog.MaterialAlertDialogBuilder;
import com.google.android.material.navigation.NavigationView;

import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.Objects;

import be.helha.comptesjustes.domain.KeyHandler;
import be.helha.comptesjustes.dto.account.DtoAccount;
import be.helha.comptesjustes.dto.transaction.DtoTransaction;
import be.helha.comptesjustes.dto.user.DtoUser;
import be.helha.comptesjustes.ui.account.AccountListFragment;
import be.helha.comptesjustes.ui.home.HomeFragment;
import be.helha.comptesjustes.ui.bill.BillsFragment;
import be.helha.comptesjustes.ui.profile.ProfileFragment;
import be.helha.comptesjustes.ui.transaction.TransactionDetailFragment;
import be.helha.comptesjustes.ui.transaction.TransactionListFragment;
import be.helha.comptesjustes.ui.user.UserDetailFragment;
import be.helha.comptesjustes.ui.user.UserListFragment;
import io.jsonwebtoken.JwtException;
import io.jsonwebtoken.security.WeakKeyException;

public class MainActivity extends AppCompatActivity
        implements
        UserListFragment.OnUserClickListener,
        AccountListFragment.OnAccountClickListener,
        TransactionListFragment.OnTransactionClickListener
{
    private DrawerLayout drawerLayout;
    private NavigationView navigationView;
    private BottomNavigationView bottomNavigationView;
    private HomeFragment homeFragment;
    private AccountListFragment accountListFragment;
    private BillsFragment billsFragment;
    private UserListFragment userListFragment;
    private ProfileFragment profileFragment;
    private DtoUser user;
    private static final int MENU_ITEM_LOG_OUT = 1;

    @Override
    protected void onCreate( Bundle savedInstanceState )
    {
        super.onCreate( savedInstanceState );
        setContentView( R.layout.activity_main );

        initViewInstances();
        initListeners();
    }

    @Override
    protected void onResume()
    {
        super.onResume();

        SharedPreferences preferences = getSharedPreferences( "user", Context.MODE_PRIVATE );

        Menu sideNavMenu = navigationView.getMenu();
        Menu bottomNavMenu = bottomNavigationView.getMenu();

        if( preferences.contains( "userId" ) )
        {
            sideNavMenu.removeItem( R.id.item_sideNavMenu_login );
            if( sideNavMenu.size() <= MENU_ITEM_LOG_OUT )
            {
                sideNavMenu.add( Menu.NONE, MENU_ITEM_LOG_OUT,Menu.NONE, "Log out" ).setIcon( R.drawable.ic_home_24dp_light );
            }
            Log.d( "Log Out Item", sideNavMenu.getItem( MENU_ITEM_LOG_OUT ).toString() );
            bottomNavMenu.findItem( R.id.navigation_users ).setVisible( true );
            bottomNavMenu.findItem( R.id.navigation_profile ).setVisible( true );
            bottomNavMenu.findItem( R.id.navigation_accounts ).setVisible( true );
            accountListFragment = AccountListFragment.newInstance( preferences.getInt( "userId", 0 ) );
            profileFragment = ProfileFragment.newInstance( preferences.getInt( "userId", 0 ) );
        }
        else
        {
            bottomNavMenu.findItem( R.id.navigation_users ).setVisible( false );
            bottomNavMenu.findItem( R.id.navigation_profile ).setVisible( false );
            bottomNavMenu.findItem( R.id.navigation_accounts ).setVisible( false );
        }
    }

    private void initViewInstances()
    {
        drawerLayout = findViewById( R.id.drawer_layout );
        Toolbar topToolbar = ( Toolbar ) findViewById( R.id.toolbar_mainActivity_top );
        setSupportActionBar( topToolbar );
        navigationView = findViewById( R.id.nv_mainActivity_drawer );
        bottomNavigationView = findViewById( R.id.bnv_mainActivity_bottom );
        homeFragment = new HomeFragment();
        billsFragment = new BillsFragment();
        userListFragment = UserListFragment.newInstance();
        bottomNavigationView.setSelectedItemId( R.id.navigation_home );

        try
        {
            KeyHandler keyHandler = new KeyHandler();

            String email = "giorgiocaculli@gmail.com";
            String password = "012345678998765432100123465789";

            byte[] passwordBytes = password.getBytes( StandardCharsets.UTF_8 );

            Log.d( "Password Bytes", Arrays.toString( passwordBytes ) );

            byte[] encodedPassword = Base64.encode( passwordBytes, Base64.DEFAULT );

            Log.d( "Encoded Password", Arrays.toString( encodedPassword ) );

            byte[] decodedPassword = Base64.decode( encodedPassword, Base64.DEFAULT );

            Log.d( "Decoded Password", Arrays.toString( decodedPassword ) );

            String jwt = keyHandler.createJsonWebToken( email, encodedPassword );

            Log.d( "Created JWT", jwt );
        } catch ( WeakKeyException wke )
        {
            Toast.makeText( getApplicationContext(), "Weak Password", Toast.LENGTH_SHORT ).show();
            wke.printStackTrace();
        }
        catch ( JwtException je )
        {
            je.printStackTrace();
        }
    }

    private void initListeners()
    {
        Objects.requireNonNull( getSupportActionBar() ).setDisplayHomeAsUpEnabled( true );
        ActionBarDrawerToggle actionBarDrawerToggle = new ActionBarDrawerToggle(
                this, drawerLayout, R.string.navigation_drawer_open, R.string.navigation_drawer_close );
        actionBarDrawerToggle.setDrawerIndicatorEnabled( false );
        actionBarDrawerToggle.setHomeAsUpIndicator( R.drawable.ic_login_24dp_dark );
        drawerLayout.addDrawerListener( actionBarDrawerToggle );
        actionBarDrawerToggle.syncState();
        bottomNavigationView.setOnItemSelectedListener( item ->
        {
            final int id = item.getItemId();
            if( id == R.id.navigation_home )
            {
                getSupportFragmentManager().beginTransaction().replace( R.id.fcv_activityMain_host, homeFragment ).commit();
                return true;
            }
            else if( id == R.id.navigation_accounts )
            {
                getSupportFragmentManager().beginTransaction().replace( R.id.fcv_activityMain_host, accountListFragment ).commit();
                return true;
            }
            else if( id == R.id.navigation_profile )
            {
                getSupportFragmentManager().beginTransaction().replace( R.id.fcv_activityMain_host, profileFragment ).commit();
                return true;
            }
            else if( id == R.id.navigation_users )
            {
                getSupportFragmentManager().beginTransaction().replace( R.id.fcv_activityMain_host, userListFragment ).commit();
                return true;
            }
            return false;
        } );

        navigationView.setNavigationItemSelectedListener( item ->
        {
            final int id = item.getItemId();
            Log.d( "Item Clicked", String.valueOf( id ) );
            if( id == R.id.item_sideNavMenu_about )
            {
                MaterialAlertDialogBuilder builder = new MaterialAlertDialogBuilder( MainActivity.this );
                builder.setTitle( "About the application" )
                        .setMessage( R.string.dialog_mainActivity_info )
                        .setPositiveButton( "Understood", null )
                        .show();
            }
            else if( id == R.id.item_sideNavMenu_login )
            {
                Intent goToLoginActivity = new Intent( MainActivity.this, LoginActivity.class );
                startActivity( goToLoginActivity );
            }
            else if( id == navigationView.getMenu().getItem( MENU_ITEM_LOG_OUT ).getItemId() )
            {
                bottomNavigationView.setSelectedItemId( R.id.navigation_home );
                bottomNavigationView.getMenu().findItem( R.id.navigation_users ).setVisible( false );
                bottomNavigationView.getMenu().findItem( R.id.navigation_profile ).setVisible( false );
                bottomNavigationView.getMenu().findItem( R.id.navigation_accounts ).setVisible( false );
                getSharedPreferences( "user", Context.MODE_PRIVATE ).edit().remove( "userId" ).apply();
                Toast.makeText( getApplicationContext(), "Logged out, please restart the app", Toast.LENGTH_LONG ).show();
            }
            item.setChecked( false );
            drawerLayout.closeDrawers();
            return true;
        } );
        drawerLayout.addDrawerListener( new DrawerLayout.DrawerListener()
        {
            @Override
            public void onDrawerSlide( @NonNull View drawerView, float slideOffset )
            {
            }

            @Override
            public void onDrawerOpened( @NonNull View drawerView )
            {
                Log.i( "Drawer Log", "Opened" );
                navigationView.bringToFront();
            }

            @Override
            public void onDrawerClosed( @NonNull View drawerView )
            {
                Log.i( "Drawer Log", "Closed" );
                int size = navigationView.getMenu().size();
                for( int i = 0; i < size; i++ )
                {
                    navigationView.getMenu().getItem( i ).setChecked( false );
                }
            }

            @Override
            public void onDrawerStateChanged( int newState )
            {

            }
        } );
    }

    @Override
    public void onBackPressed()
    {
        if( drawerLayout.isDrawerOpen( GravityCompat.START ) )
        {
            drawerLayout.closeDrawer( GravityCompat.START );
        }
        super.onBackPressed();
    }

    @Override
    public boolean onOptionsItemSelected( @NonNull MenuItem item )
    {
        final int id = item.getItemId();
        if( id == android.R.id.home )
        {
            drawerLayout.openDrawer( GravityCompat.START );
            return true;
        }
        return super.onOptionsItemSelected( item );
    }

    @Override
    public void onUserClick( DtoUser dtoUser )
    {
        Log.i( "User Log: ", dtoUser.toString() );
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction transaction = fragmentManager.beginTransaction();
        transaction.addToBackStack( "user" );
        transaction.replace( R.id.fcv_activityMain_host, UserDetailFragment.newInstance( dtoUser.getId() ) );
        transaction.commit();
    }

    @Override
    public void onTransactionClick( DtoTransaction dtoTransaction )
    {
        Log.i( "Transaction Log: ", dtoTransaction.toString() );
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction transaction = fragmentManager.beginTransaction();
        transaction.addToBackStack( "transaction" );
        transaction.replace( R.id.fcv_activityMain_host, TransactionDetailFragment.newInstance( dtoTransaction.getId() ) );
        transaction.commit();
    }

    @Override
    public void onAccountClick( DtoAccount dtoAccount )
    {
        Log.i( "Account Log: ", dtoAccount.toString() );
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction transaction = fragmentManager.beginTransaction();
        transaction.addToBackStack( "account" );
        transaction.replace( R.id.fcv_activityMain_host, TransactionListFragment.newInstance( dtoAccount.getId() ) );
        transaction.commit();
    }
}
